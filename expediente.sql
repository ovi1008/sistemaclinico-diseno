-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-09-2019 a las 20:36:15
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `expediente`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinicas`
--

CREATE TABLE `clinicas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `clinicaNombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clinicaDescripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clinicaDireccion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clinicaTelefono` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clinicaImagen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `clinicaLogo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `clinicas`
--

INSERT INTO `clinicas` (`id`, `clinicaNombre`, `clinicaDescripcion`, `clinicaDireccion`, `clinicaTelefono`, `clinicaImagen`, `clinicaLogo`, `created_at`, `updated_at`) VALUES
(1, 'Clinica 7', 'descripcion larga de la clinica 7', 'direccion de la clinica 7', '4444-4567', '41_prueba.jpg', '41_Screenshot_20190928-044946_Adobe Acrobat.jpg', '2019-09-23 06:00:00', '2019-09-29 06:35:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_08_19_000000_create_failed_jobs_table', 1),
(7, '2019_09_23_231404_clinicas', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jorge.ovi10@gmail.com', '$2y$10$Z1vmaHZYF.sJuNUYt7MBlegGL3MUubKpj0JaVfASJ0UM.Dt7bnaCO', '2019-09-30 00:32:44');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `usuarioAvatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'usuario_default.png',
  `usuarioNombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuarioNombre2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuarioNombre3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuarioApellido` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuarioApellido2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuarioApellido3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuarioDui` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuarioNit` varchar(17) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `usuarioTipo` enum('0','1','2','3') COLLATE utf8mb4_unicode_ci NOT NULL,
  `usuarioJrv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usuarioEstado` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `usuarioAvatar`, `usuarioNombre`, `usuarioNombre2`, `usuarioNombre3`, `usuarioApellido`, `usuarioApellido2`, `usuarioApellido3`, `usuarioDui`, `usuarioNit`, `email`, `email_verified_at`, `usuarioTipo`, `usuarioJrv`, `usuarioEstado`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '54_prueba.jpg', 'Prueba', NULL, NULL, 'adsi1xddd', NULL, NULL, '45494545-0', '6767-891998-999-0', 'jorge.ovi10@gmail.com', NULL, '0', '23132', '1', '$2y$10$/0ZTbSTfpWR.gO2ZuXcUDOGEHmJvEmKZeNCQv3AQp9jV.Y4lM9jyi', '83Afv6WcdqewF6qjgjpQQV6M4RSapZL1kEr95Ir05rBMXfvtan7RsjRzhUjr', NULL, '2019-09-29 22:50:56'),
(2, 'usuario_default.png', 'nomSolA', NULL, NULL, 'apelSolA', NULL, NULL, '74578345-5', '1234-123456-123-1', 'prueba002Dsi115@gmail.com', NULL, '0', '23423', '1', '$2y$10$8MIpY7B9pphW4YGhonubLeoEfTjdLCjxsLb0UAlS6MFok5nld.WGq', NULL, '2019-09-28 02:16:35', '2019-09-28 02:16:35'),
(4, '30_prueba.jpg', 'nomA', 'nomb', 'nomc', 'apeA', NULL, NULL, '12378623-4', '1234-123456-123-2', 'prueba003Dsi115@gmail.com', NULL, '3', NULL, '0', '$2y$10$0feW9V7JXm3IguoI4f9ZAeXLTIMQn.EzvBcmISV6VGmVj.p0LSSs.', NULL, '2019-09-28 02:33:30', '2019-09-28 07:49:26'),
(5, '29_WhatsApp Image 2019-09-24 at 10.30.04 AM.jpeg', 'nomAx', 'nomBx', 'nomCx', 'apeAx', 'apeBx', 'apeCx', '78234783-7', '1234-123456-122-7', '7x2prueba004Dsi115@gmail.com', NULL, '1', '345377', '1', '$2y$10$6hTlPaJP3.hFV3GFWqwZ7.4FRhOi6L6pJuAg.LEzi02/kw5SBbIq2', NULL, '2019-09-28 02:41:18', '2019-09-29 06:49:29'),
(6, '29_WhatsApp Image 2019-09-24 at 10.30.04 AM.jpeg', 'ansbs', NULL, NULL, 'dfgj', NULL, NULL, '35776845-5', '1234-123456-123-9', 'prueba005Dsi115@gmail.com', NULL, '0', NULL, '1', '$2y$10$J.BocLVmi0q1ZfRAMupvTuau86WroGKOOm5EFQ7sBV3BM6ezYQIQS', NULL, '2019-09-28 03:31:29', '2019-09-28 03:31:29'),
(7, '33_magento-error.PNG', 'fdgf', NULL, NULL, 'fhfj', NULL, NULL, '12312423-5', '1234-123456-123-8', 'prueba006Dsi115@gmail.com', NULL, '2', NULL, '1', '$2y$10$7On.dpjPsZbWlmXL44.nu.HkilvuABEjjTvCEFI.AS51Kvxxve7cq', NULL, '2019-09-28 03:38:33', '2019-09-29 06:56:29'),
(8, 'usuario_default.png', 'sdfgbjhdf', 'ndfjkndfjk', 'ndfjkgnd', 'dnjkdfn', 'dfngjkdf', 'lkdfngkjdf', '45477678-8', '1234-098765-123-2', 'prueba009Dsi115@gmail.com', NULL, '0', '4354', '1', '$2y$10$k3XDGdh6obcL5AWAzmHMr.aGgA1ku2A4QcfaCxt.5BdBgXWinICYe', NULL, '2019-09-28 08:10:23', '2019-09-28 08:10:23');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clinicas`
--
ALTER TABLE `clinicas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_usuariodui_unique` (`usuarioDui`),
  ADD UNIQUE KEY `users_usuarionit_unique` (`usuarioNit`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clinicas`
--
ALTER TABLE `clinicas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
