<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    protected $table='receta';
    protected $fillable = [
        'recetaMedicamento', 'recetaObservaciones',  
    ];

    public $timestamps = false;

}
