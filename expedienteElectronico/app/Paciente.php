<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table='pacientes';
    protected $fillable = [
        'pacienteNombre1','pacienteNombre2','pacienteNombre3','pacienteApellido1','pacienteApellido2',
        'pacienteApellido3', 'pacienteSexo','pacienteSexo','pacienteReligion','pacienteFechaNacimiento',
        'pacienteTelefono', 'pacienteDireccion','pacienteProfesion','pacienteDui', 'pacienteNit',
        'pacientePersonaEmergencia', 'pacientePersonaEmergenciaTelefono','pacienteTipoDeSangre',
        'pacienteCodigo','pacienteDEpartamento','pacienteMunicipio','pacienteEstadoCivil',
    ];
}


