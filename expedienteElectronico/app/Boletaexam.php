<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boletaexam extends Model
{
    protected $table='boletaexam';
    protected $fillable = [
        'boletaexamSolicitados',  
    ];

    public $timestamps = false;

}
