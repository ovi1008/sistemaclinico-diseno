<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    protected $table='consultas';
    protected $fillable = [
        'idPacientePreparacion', 'consultaConsultaPor','consultaPresentaEnfermedad',
        'consultaAntecedentesPersonales','consultaAntecedentesFamiliares','consultaExploracionClinica',
        'consultaDiagnosticoPrincipal', 'consultaOtrosDiagnosticos', 'consultaTratamiento',
        'consultaObservaciones', 'consultaEnfermedadP', 'consultaEnfermedadS1', 'consultaEnfermedadS2',  
    ];
    ///preubaa


}

