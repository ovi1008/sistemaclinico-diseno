<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asueto extends Model
{
    protected $table='asueto';
    protected $fillable = [
        'asuetoNombreDia', 'asuetoFecha',  
    ];

    public $timestamps = false;
}
