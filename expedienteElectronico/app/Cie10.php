<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cie10 extends Model
{
    protected $table='cie10';
    protected $fillable = [
        'cie10Codigo', 'cie10Descripcion', 
        'cie10LimiteInferior', 'cie10LimiteSuperior', 'cie10Sexo', 
    ];

    public $timestamps = false;
}
