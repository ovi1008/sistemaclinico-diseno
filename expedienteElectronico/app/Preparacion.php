<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preparacion extends Model
{
    protected $table='preparaciones';
    protected $fillable = [
        'idPaciente','preparacionPulso','preparacionTemperatura',
        'preparacionPeso','preparacionAltura','preparacionAsignacion',
        'preparacionEstado'
    ];
}

