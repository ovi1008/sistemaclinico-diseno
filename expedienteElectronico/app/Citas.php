<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citas extends Model
{
    protected $table='citas';
    protected $fillable = [
        'idDoctor', 'idPaciente','fechaCita' ,'horaCita',
    ];

    public function Paciente()
    {
        return $this->belongsTo('App\Paciente');
    }

   // public function Doctor()
    //{
    //    return $this->belongsTo('App\Doctor');
    //}
}

