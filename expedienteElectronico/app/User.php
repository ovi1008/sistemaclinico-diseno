<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='users';
    protected $fillable = [
        'usuarioAvatar', 'usuarioNombre', 'usuarioNombre2', 'usuarioNombre3',
        'usuarioApellido', 'usuarioApellido2', 'usuarioApellido3', 
        'usuarioDui', 'usuarioNit', 'email', 'password',
        'usuarioTipo', 'usuarioJrv', 'usuarioEstado', 'idEspecialidad',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function sendPasswordResetNotification($token)
{
    $this->notify(new ResetPassword($token));
}
}
