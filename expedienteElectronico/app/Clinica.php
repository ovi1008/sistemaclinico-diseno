<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    //
    protected $table='clinicas';
    protected $fillable = [
        'clinicaNombre', 'clinicaDescripcion', 
        'clinicaDireccion', 'clinicaTelefono', 'clinicaImagen', 'clinicaLogo',
    ];
}
