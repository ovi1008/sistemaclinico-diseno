<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EspecialidadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'especialidadNombre' => ['required', 'string', 'max:30', 'unique:especialidades'],
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'especialidadNombre' => 'required', 'string', 'max:30','unique:especialidades'.$this->get('id'),
                    ];
            }
            default:break;
        }
    
    }
}
