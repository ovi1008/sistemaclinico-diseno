<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PacienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'pacienteNombre1' => ['required', 'string', 'max:30'],
                    'pacienteNombre2' => ['nullable','string', 'max:30'],
                    'pacienteNombre3' => ['nullable','string', 'max:30'],
                    'pacienteApellido1' => ['required', 'string', 'max:30'],
                    'pacienteApellido2' => ['nullable','string', 'max:30'],
                    'pacienteApellido3' => ['nullable','string', 'max:30'],
                    'pacienteSexo' => ['required','string', 'max:10'],
                    'pacienteReligion' => ['nullable','string', 'max:50'],
                    'pacienteFechaNacimiento' => ['required','string', 'max:10'],
                    'pacienteTelefono' => ['required','string', 'max:15'],
                    'pacienteDireccion' => ['required','string', 'max:100'],
                    'pacienteProfesion' => ['nullable','string', 'max:50'],
                    'pacienteDui' => ['nullable','string', 'max:10', 'unique:pacientes'],
                    'pacienteNit' => ['nullable','string', 'max:17', 'unique:pacientes'],
                    'pacientePersonaEmergencia' => ['nullable','string', 'max:60'],
                    'pacientePersonaEmergenciaTelefono' => ['nullable','string', 'max:10'],
                    'pacienteTipoDeSangre' => ['nullable','string', 'max:10'],
                    'pacienteMunicipio' => ['required','string', 'max:10'],
                    'pacienteDepartamento' => ['required','string', 'max:3'],
                    'pacienteEstadoCivil' => ['required','string', 'max:3'],
                    
                ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'pacienteNombre1' => ['required', 'string', 'max:30'],
                    'pacienteNombre2' => ['nullable','string', 'max:30'],
                    'pacienteNombre3' => ['nullable','string', 'max:30'],
                    'pacienteApellido1' => ['required', 'string', 'max:30'],
                    'pacienteApellido2' => ['nullable','string', 'max:30'],
                    'pacienteApellido3' => ['nullable','string', 'max:30'],
                    'pacienteSexo' => ['required','string', 'max:10'],
                    'pacienteReligion' => ['nullable','string', 'max:50'],
                    'pacienteFechaNacimiento' => ['required','string', 'max:10'],
                    'pacienteTelefono' => ['required','string', 'max:15'],
                    'pacienteDireccion' => ['required','string', 'max:100'],
                    'pacienteProfesion' => ['nullable','string', 'max:50'],
                    'pacienteDui' => 'nullable','string', 'max:10', 'unique:pacientes,pacienteDui'.$this->get('id'),
                    'pacienteNit' => 'nullable','string', 'max:17', 'unique:pacientes,pacienteNit'.$this->get('id'),
                    'pacientePersonaEmergencia' => ['nullable','string', 'max:60'],
                    'pacientePersonaEmergenciaTelefono' => ['nullable','string', 'max:10'],
                    'pacienteTipoDeSangre' => ['nullable','string', 'max:10'],
                    'pacienteMunicipio' => ['required','string', 'max:10'],
                    'pacienteDepartamento' => ['required','string', 'max:3'],
                    'pacienteEstadoCivil' => ['required','string', 'max:3'],

                ];
            }
            default:break;
        }
    }
}
