<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsultaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'consultaPresentaEnfermedad' => ['nullable', 'string', 'max:190'],
                    'consultaConsultaPor' => ['required','string', 'max:190'],
                    'consultaAntecedentesPersonales' => ['nullable','string', 'max:190'],
                    'consultaAntecedentesFamiliares' => ['nullable', 'string', 'max:190'],
                    'consultaExploracionClinica' => ['required','string', 'max:190'],
                    'consultaDiagnosticoPrincipal' => ['required','string', 'max:190'], 
                    'consultaOtrosDiagnosticos' => ['nullable','string', 'max:190'], 
                    'consultaTratamiento' => ['required','string', 'max:190'], 
                    'consultaObservaciones' => ['nullable','string', 'max:190'],
                    'consultaEnfermedadP' => ['required', 'string', 'max:190'], 
                    'consultaEnfermedadS1' => ['nullable', 'string', 'max:190'],
                    'consultaEnfermedadS2' => ['nullable', 'string', 'max:190'],                      
                ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'consultaPresentaEnfermedad' => ['nullable', 'string', 'max:190'],
                    'consultaConsultaPor' => ['required','string', 'max:190'],
                    'consultaAntecedentesPersonales' => ['nullable','string', 'max:190'],
                    'consultaAntecedentesFamiliares' => ['nullable', 'string', 'max:190'],
                    'consultaExploracionClinica' => ['required','string', 'max:190'],
                    'consultaDiagnosticoPrincipal' => ['required','string', 'max:190'], 
                    'consultaOtrosDiagnosticos' => ['nullable','string', 'max:190'], 
                    'consultaTratamiento' => ['required','string', 'max:190'], 
                    'consultaObservaciones' => ['nullable','string', 'max:190'],
                    'consultaEnfermedadP' => ['required', 'string', 'max:190'], 
                    'consultaEnfermedadS1' => ['nullable', 'string', 'max:190'],
                    'consultaEnfermedadS2' => ['nullable', 'string', 'max:190'],     
                    
                ];
            }
            default:break;
        }
    }
}
