<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'usuarioAvatar' => ['mimes:png,jpg,jpeg'],
                    'usuarioNombre' => ['required', 'string', 'max:30'],
                    'usuarioNombre2' => ['nullable', 'string', 'max:30'],
                    'usuarioNombre3' => ['nullable', 'string', 'max:30'],
                    'usuarioApellido' => ['required', 'string', 'max:30'],
                    'usuarioApellido2' => ['nullable', 'string', 'max:30'],
                    'usuarioApellido3' => ['nullable', 'string', 'max:30'],
                    'usuarioDui' => ['required', 'string', 'max:10','unique:users'],
                    'usuarioNit' => ['required', 'string', 'max:17','unique:users'],
                    'usuarioJrv' => ['nullable', 'string', 'max:255','unique:users'],
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                    'usuarioTipo' => ['required', 'string'],
                    'idEspecialidad' => ['nullable', 'string'],
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'usuarioAvatar' => ['mimes:png,jpg,jpeg'],
                    'usuarioNombre' => ['required', 'string', 'max:30'],
                    'usuarioNombre2' => ['nullable', 'string', 'max:30'],
                    'usuarioNombre3' => ['nullable', 'string', 'max:30'],
                    'usuarioApellido' => ['required', 'string', 'max:30'],
                    'usuarioApellido2' => ['nullable', 'string', 'max:30'],
                    'usuarioApellido3' => ['nullable', 'string', 'max:30'],
                    'usuarioDui' => 'required', 'string', 'max:10','unique:users,usuarioDui,'.$this->get('id'),
                    'usuarioNit' => 'required', 'string', 'max:17','unique:users,usuarioNit,'.$this->get('id'),
                    'usuarioJrv' => 'nullable', 'string', 'max:255','unique:users,usuarioJrv,'.$this->get('id'),
                    'email' => 'required|string|email|max:255|unique:users,email,'.$this->get('id'),
                    'usuarioTipo' => ['required', 'string'],
                    'idEspecialidad' => ['nullable', 'string'],
                ];
            }
            default:break;
        }
    }
    public function messages()
    {
        return [
            
            'usuarioNombre.required' =>'El nombre es requerido.',
            'name.required' =>'El nombre es requerido.',
            'email.required'=>'Correo electronico es requerido.',
            'email.email'=>'Correo electronico no es valido.',
            'email.unique'=>'Correo electronico ya existe.',
            
        ];
    }
}
