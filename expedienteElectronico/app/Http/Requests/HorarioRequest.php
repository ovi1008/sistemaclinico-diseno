<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HorarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'horarioHoraI' => ['required', 'string', 'max:5'],
                    'horarioHoraF' => ['required', 'string', 'max:5'],
                    'horarioPaciente' => ['required', 'string', 'max:2'],
                    'horarioDoctorId' => ['required', 'string', 'unique:horarios'],
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'horarioHoraI' => ['required', 'string', 'max:5'],
                    'horarioHoraF' => ['required', 'string', 'max:5'],
                    'horarioPaciente' => ['required', 'string', 'max:2'],
                    ];

            }
            default:break;
        }
    }
}
