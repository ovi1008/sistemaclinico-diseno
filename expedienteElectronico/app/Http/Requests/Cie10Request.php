<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Cie10Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'cie10Codigo' => ['required', 'string', 'max:15', 'unique:cie10'],
                    'cie10Descripcion' => ['required', 'string', 'max:150'],
                    'cie10LimiteInferior' => ['required', 'string', 'max:3'],
                    'cie10LimiteSuperior' => ['required', 'string', 'max:3'],
                    'cie10Sexo' => ['required', 'string', 'max:1'],
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'cie10Codigo' => 'required', 'string', 'max:15','unique:cie10'.$this->get('id'),
                    'cie10Descripcion' => ['required', 'string', 'max:150'],
                    'cie10LimiteInferior' => ['required', 'string', 'max:3'],
                    'cie10LimiteSuperior' => ['required', 'string', 'max:3'],
                    'cie10Sexo' => ['required', 'string', 'max:1'],
                    ];
            }
            default:break;
        }
    }
}
