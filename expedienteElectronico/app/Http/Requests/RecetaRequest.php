<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RecetaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'recetaMedicamento' => ['required', 'string', 'max:250'],
                    'recetaObservaciones' => ['required', 'string', 'max:250'],
                    
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'recetaMedicamento' => ['required', 'string', 'max:250'],
                    'recetaObservaciones' => ['required', 'string', 'max:250'],
                    ];
            }
            default:break;
        }
    }
}
