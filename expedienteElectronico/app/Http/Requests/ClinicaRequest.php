<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClinicaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'clinicaNombre' => ['required', 'string', 'max:255'],
            'clinicaDescripcion' => ['required', 'string', 'max:255'],
            'clinicaDireccion' => ['required', 'string', 'max:255'],
            'clinicaTelefono' => ['required', 'string', 'max:9'],
            'clinicaImagen' => ['mimes:png,jpg,jpeg'],
            'clinicaLogo' => ['mimes:png,jpg,jpeg'],
            
        ];
    }
}
