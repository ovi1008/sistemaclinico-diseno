<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CitasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {               
                return [
                    //
                    'idDoctor' => ['required', 'string', 'max:10'],
                    'idPaciente' => ['required', 'string', 'max:10'],
                    'fechaCita' => ['required', 'string', 'max:10'],
                    'horaCita' => ['required', 'string', 'max:5'],
                    
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'idDoctor' => ['required', 'string', 'max:10'],
                    'idPaciente' => ['required', 'string', 'max:10'],
                    'fechaCita' => ['required', 'string', 'max:10'],
                    'horaCita' => ['required', 'string', 'max:5'],
                    ];
            }
            default:break;
        }
    }
}
