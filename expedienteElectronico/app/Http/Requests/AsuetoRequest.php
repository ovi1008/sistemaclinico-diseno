<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AsuetoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'asuetoNombreDia' => ['required', 'string', 'max:50'],
                    'asuetoFecha' => ['required', 'string', 'max:5'],
                    
                    ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'asuetoNombreDia' => ['required', 'string', 'max:50'],
                    'asuetoFecha' => ['required', 'string', 'max:5'],
                    ];
            }
            default:break;
        }
    }
}
