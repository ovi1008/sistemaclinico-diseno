<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProcedimientoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                return [
                    //
                    'preparacionPulso' => ['nullable', 'string', 'max:10'],
                    'preparacionTemperatura' => ['nullable','string', 'max:10'],
                    'preparacionPeso' => ['nullable','string', 'max:10'],
                    'preparacionAltura' => ['nullable', 'string', 'max:10'],
                    'preparacionAsignacion' => ['required','string', 'max:10'],                    
                ];
            }
            case 'PATCH':
            {
                return [
                    //
                    'preparacionPulso' => ['nullable', 'string', 'max:10'],
                    'preparacionTemperatura' => ['nullable','string', 'max:10'],
                    'preparacionPeso' => ['nullable','string', 'max:10'],
                    'preparacionAltura' => ['nullable', 'string', 'max:10'],
                    'preparacionAsignacion' => ['required','string', 'max:10'],
                    
                ];
            }
            default:break;
        }
    }
}
