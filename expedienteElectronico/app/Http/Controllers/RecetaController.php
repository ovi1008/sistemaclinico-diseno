<?php

namespace App\Http\Controllers;

use App\Receta;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\RecetaRequest;
use DB;
use Session;
use App\User;




class RecetaController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$receta = DB::table('receta')->where('recetaSexo', '=', "1")->get();
        $receta = DB::table('receta')->get();
        //dd($receta);
    	return view('receta/recetaList',compact('receta'));
        }
        else{
            return view('/');
        }    
    }
//show
public function show(Request $request, $id)
{
    //
    if($request->user()->usuarioTipo=='0'){
    $receta = Receta::findorFail($id);
    //dd($receta);
    return view('receta/recetaDetalle',compact('receta'));
    }
    else{
        return view('/');
    }
}

//llama la vista de crear
public function create(Request $request)
{
    if($request->user()->usuarioTipo=='0'){
    return view('receta/recetaNew'); 
}
else{
    return view('/');
}
}

public function store(Request $request,RecetaRequest $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $receta= new Receta;
            $receta->recetaMedicamento=$rqPaci->get('recetaMedicamento');
            $receta->recetaObservaciones=$rqPaci->get('recetaObservaciones');
            $receta->save();
            return redirect('/receta/'.$receta->id);
        }
        else{
            return view('/');
        }
        
    }

//actualizar
public function edit(Request $request, $id)
    {
        //
        $receta=Receta::findorFail($id);          
        //dd($receta->recetaMedicamento);  
        return view('receta/recetaEdit',compact('receta'));
    }

public function update(Request $request, RecetaRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $receta= Receta::findOrFail($id);
            $receta->recetaMedicamento=$parq->get('recetaMedicamento');
            $receta->recetaObservaciones=$parq->get('recetaObservaciones');
            $receta->update();
            return redirect('/receta/'.$receta->id);
        }
        else{
            return view('/');
        }
         }

//eliminar
public function destroy(Request $request, $id){
//dd($id);
if($request->user()->usuarioTipo=='0'){
    Receta::destroy($id);
    Session::flash('status','¡Receta, eliminada con exito.');
    return redirect()->action('RecetaController@index');
   }
   else{
    return view('/');
   }
}

}
