<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PacienteRequest;
use DB;
use App\Paciente;
use App\Preparacion;
use Carbon\Carbon;
use Session; 




class PacienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index()
    {
        //
        $paciente = DB::table('pacientes')->get();
        foreach($paciente as $indice=>$pacienteA){
            $fecha = explode("/", $pacienteA->pacienteFechaNacimiento);
            $edadV[$indice]=Carbon::createFromDate($fecha[2], $fecha[1], $fecha[0])->age;
        }
    	return view('paciente/pacienteList',compact('paciente', 'edadV'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='3')){
            $departamentos = DB::table('depsv')->get();
            $sangres = DB::table('tiposangre')->get();
    		return view('paciente/pacienteNew',['departamentos'=>$departamentos,'sangres'=>$sangres]);
        }
        return redirect()->action('PacienteController@index');

    }

    public function obtenerMunicipios(Request $request, $id){
        if($request->ajax()){
            $municipios = DB::table('munsv')->where('DEPSV_ID','=',$id)->get();
            return response()->json($municipios);
        }

    }

    public function obtenerMunicipios2(Request $request, $id, $id2){
        if($request->ajax()){
            $municipios = DB::table('munsv')->where('DEPSV_ID','=',$id2)->get();
            return response()->json($municipios);
        }

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,PacienteRequest $rqPaci)
    {
        //
        $pacU=DB::table('pacientes')->orderBy('id', 'desc')->first();
        $fecha = Carbon::now();
        $codFecha=strtotime($fecha);
        $codStr=(string)$codFecha;
        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='3')){
            $paciente= new Paciente;
            $paciente->pacienteCodigo=$codStr.$pacU->id;
            $paciente->pacienteNombre1=$rqPaci->get('pacienteNombre1');
            $paciente->pacienteNombre2=$rqPaci->get('pacienteNombre2');
            $paciente->pacienteNombre3=$rqPaci->get('pacienteNombre3');
            $paciente->pacienteApellido1=$rqPaci->get('pacienteApellido1');
            $paciente->pacienteApellido2=$rqPaci->get('pacienteApellido2');
            $paciente->pacienteApellido3=$rqPaci->get('pacienteApellido3');
            $paciente->pacienteSexo=$rqPaci->get('pacienteSexo');
            $paciente->pacienteReligion=$rqPaci->get('pacienteReligion');
            $paciente->pacienteFechaNacimiento=$rqPaci->get('pacienteFechaNacimiento');
            $paciente->pacienteTelefono=$rqPaci->get('pacienteTelefono');
            $paciente->pacienteDireccion=$rqPaci->get('pacienteDireccion');
            $paciente->pacienteProfesion=$rqPaci->get('pacienteProfesion');
            $paciente->pacienteDui=$rqPaci->get('pacienteDui');
            $paciente->pacienteNit=$rqPaci->get('pacienteNit');
            $paciente->pacientePersonaEmergencia=$rqPaci->get('pacientePersonaEmergencia');
            $paciente->pacientePersonaEmergenciaTelefono=$rqPaci->get('pacientePersonaEmergenciaTelefono');
            $paciente->pacienteTipoDeSangre=$rqPaci->get('pacienteTipoDeSangre');
            $paciente->pacienteDepartamento=$rqPaci->get('pacienteDepartamento');
            $paciente->pacienteMunicipio=$rqPaci->get('pacienteMunicipio');
            $paciente->pacienteEstadoCivil=$rqPaci->get('pacienteEstadoCivil');
            $paciente->save();
            return redirect('/paciente/'.$paciente->id);
        }
        return redirect()->action('PacienteController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $paciente = Paciente::findorFail($id);
        $departamento=DB::table('depsv')->where('ID','=',$paciente->pacienteDepartamento)->first();
        $municipio = DB::table('munsv')->where('ID','=',$paciente->pacienteMunicipio)->first();
		return view('paciente/pacienteDetalle',compact('paciente', 'departamento', 'municipio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Paciente $paciente)
    {
        //
        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='3')){
            $departamentos = DB::table('depsv')->get();
            $sangres = DB::table('tiposangre')->get();
            $municipios = DB::table('munsv')->where('DEPSV_ID','=',$paciente->pacienteDepartamento)->get();
            //dd($paciente);
            return view('paciente/pacienteEdit',compact('paciente','departamentos','municipios', 'sangres'));
        }
        return redirect()->action('PacienteController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PacienteRequest $parq,$id)
    {
        //
        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='3')){
            $paciente= Paciente::findOrFail($id);
            $paciente->pacienteNombre1=$parq->get('pacienteNombre1');
            $paciente->pacienteNombre2=$parq->get('pacienteNombre2');
            $paciente->pacienteNombre3=$parq->get('pacienteNombre3');
            $paciente->pacienteApellido1=$parq->get('pacienteApellido1');
            $paciente->pacienteApellido2=$parq->get('pacienteApellido2');
            $paciente->pacienteApellido3=$parq->get('pacienteApellido3');
            $paciente->pacienteSexo=$parq->get('pacienteSexo');
            $paciente->pacienteReligion=$parq->get('pacienteReligion');
            $paciente->pacienteFechaNacimiento=$parq->get('pacienteFechaNacimiento');
            $paciente->pacienteTelefono=$parq->get('pacienteTelefono');
            $paciente->pacienteDireccion=$parq->get('pacienteDireccion');
            $paciente->pacienteProfesion=$parq->get('pacienteProfesion');
            $paciente->pacienteDui=$parq->get('pacienteDui');
            $paciente->pacienteNit=$parq->get('pacienteNit');
            $paciente->pacientePersonaEmergencia=$parq->get('pacientePersonaEmergencia');
            $paciente->pacientePersonaEmergenciaTelefono=$parq->get('pacientePersonaEmergenciaTelefono');
            $paciente->pacienteTipoDeSangre=$parq->get('pacienteTipoDeSangre');
            $paciente->pacienteDepartamento=$parq->get('pacienteDepartamento');
            $paciente->pacienteMunicipio=$parq->get('pacienteMunicipio');
            $paciente->pacienteEstadoCivil=$parq->get('pacienteEstadoCivil');
            $paciente->update();
            return redirect('/paciente/'.$paciente->id);
        }
        return redirect()->action('PacienteController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        if(($request->user()->usuarioTipo == '0')||($request->user()->usuarioTipo == '3')){
            $cantidad = Preparacion::where('idPaciente', '=', $id)->count();
            if($cantidad==0){
                $accion= Paciente::where('id','=',$id)->delete();
                Session::flash('status','¡Paciente eliminado con exito.');
                return redirect()->action('PacienteController@index');
            }
            Session::flash('status','¡Paciente ya posee historia clinica, prohibido eliminar!');
            return redirect()->action('PacienteController@index');
        }
    	return redirect()->action('PacienteController@index'); 
    }

}
