<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Especialidad;
use DB;
use Session; 

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
		$this->middleware('auth');        
    }
    public function detalle(Request $request, $id){
        $user = User::findorFail($id);
        if (($user->usuarioTipo==0)||($user->usuarioTipo==2)) {
            $control=1;
            $esp=Especialidad::findOrFail($user->idEspecialidad);
        } else {
            $control=0;
            $esp=Especialidad::findOrFail(1);
        }
		if($request->user()->usuarioTipo == '0'){
            return view('usuario/userDetalle',compact('user','esp','control'));
            //dd($user);
		}
		return redirect('/');
	}

    public function index(Request $request)
    {
        //
        $userT = User::where('usuarioEstado', '1')->get();
    	if($request->user()->usuarioTipo == '0'){
        return view('usuario/userList',compact('userT'));

    	}
    	return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if($request->user()->usuarioTipo == '0'){
            $especialidad = DB::table('especialidades')->get();
    		return view('usuario/userNew',['especialidad'=>$especialidad]); 
    	}
    	return redirect('/');  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $requestU,UserRequest $request)
    {
        //
        if($requestU->user()->usuarioTipo == '0'){
        $usuario= new User;
        if($file=$request->file('usuarioAvatar')){
            $file->move(public_path().'/img/users',Carbon::now()->second.'_'.$file->getClientOriginalName());
			$usuario->usuarioAvatar=Carbon::now()->second.'_'.$file->getClientOriginalName();
		}
        $usuario->usuarioNombre=$request->get('usuarioNombre');
        $usuario->usuarioNombre2=$request->get('usuarioNombre2');
        $usuario->usuarioNombre3=$request->get('usuarioNombre3');
        $usuario->usuarioApellido=$request->get('usuarioApellido');
        $usuario->usuarioApellido2=$request->get('usuarioApellido2');
        $usuario->usuarioApellido3=$request->get('usuarioApellido3');
        $usuario->usuarioDui=$request->get('usuarioDui');
        $usuario->usuarioNit=$request->get('usuarioNit');
        $usuario->usuarioTipo=$request->get('usuarioTipo');
        $usuario->usuarioJrv=$request->get('usuarioJrv');
        $usuario->email=$request->get('email');
        $usuario->idEspecialidad=$request->get('idEspecialidad');
        $usuario->password=bcrypt($request->get('password')); 
        $usuario->save();
        return redirect('/user/'.$usuario->id.'/detalle');
    }
    return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        $userT = User::where('usuarioEstado', '0')->get();

    	if($request->user()->usuarioTipo == '0'){
        return view('usuario/userList2',compact('userT'));

    	}
    	return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,User $user)
    {
        //
        if($request->user()->usuarioTipo == '0'){
            $especialidad = DB::table('especialidades')->get();
            return view('usuario/userEdit',['user'=>$user],['especialidad'=>$especialidad]);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $requestU, UserRequest $request,$id)
    {
        //
        if($requestU->user()->usuarioTipo == '0'){
        $usuario=User::findOrFail($id);

        if( $file=$request->file('usuarioAvatar')){
            
            $old_image=$usuario->usuarioAvatar;
            if(strcmp($old_image,'usuario_default.png')){
                if(is_file(public_path().'/img/users/'.$old_image)){
                    unlink(public_path().'/img/users/'.$old_image);
                } 
            }
            $file->move(public_path().'/img/users',Carbon::now()->second.'_'.$file->getClientOriginalName());
            $usuario->usuarioAvatar=Carbon::now()->second.'_'.$file->getClientOriginalName();
    
         }

        $usuario->usuarioNombre=$request->get('usuarioNombre');
        $usuario->usuarioNombre2=$request->get('usuarioNombre2');
        $usuario->usuarioNombre3=$request->get('usuarioNombre3');
        $usuario->usuarioApellido=$request->get('usuarioApellido');
        $usuario->usuarioApellido2=$request->get('usuarioApellido2');
        $usuario->usuarioApellido3=$request->get('usuarioApellido3');
        $usuario->usuarioDui=$request->get('usuarioDui');
        $usuario->usuarioNit=$request->get('usuarioNit');
        $usuario->usuarioTipo=$request->get('usuarioTipo');
        $usuario->usuarioJrv=$request->get('usuarioJrv');
        $usuario->idEspecialidad=$request->get('idEspecialidad');
        $usuario->email=$request->get('email');
        $usuario->update();
        return redirect('/user/'.$usuario->id.'/detalle');
    }
    return redirect('/');
                
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function estadoC(Request $request, $id){
        if($request->user()->usuarioTipo == '0'){
            $user = User::findorFail($id);
            $user->usuarioEstado='0';
            $user->update();
            Session::flash('status','¡Usuario inactivado con exito.');
            return redirect()->action('UserController@show');
        }
        return redirect('/');
		
	}

	public function estadoC2(Request $request, $id){
		if($request->user()->usuarioTipo == '0'){
            $user = User::findorFail($id);
            $user->usuarioEstado='1';
            $user->update();
            Session::flash('status','¡Usuario activado con exito.');
            return redirect()->action('UserController@show');
        }
        return redirect('/');
	}
}
