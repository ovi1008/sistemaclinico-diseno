<?php

namespace App\Http\Controllers;

use App\Boletaexam;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BoletaexamRequest;
use DB;
use Session;
use App\User;

class BoletaexamController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }

   
    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$boletaexam = DB::table('boletaexam')->where('boletaexamSexo', '=', "1")->get();
        $boletaexam = DB::table('boletaexam')->get();
        //dd($boletaexam);
    	return view('boletaexam/boletaexamList',compact('boletaexam'));
        }
        else{
            return view('/');
        }    
    }

    //show
public function show(Request $request, $id)
{
    //
    if($request->user()->usuarioTipo=='0'){
    $boletaexam = Boletaexam::findorFail($id);
    //dd($boletaexam);
    return view('boletaexam/boletaexamDetalle',compact('boletaexam'));
    }
    else{
        return view('/');
    }
}


//llama la vista de crear
    public function create(Request $request)
    {
        if($request->user()->usuarioTipo=='0'){
        return view('boletaexam/boletaexamNew'); 
    }
    else{
        return view('/');
    }
    }
public function store(Request $request,BoletaexamRequest $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $boletaexam= new Boletaexam;
            $boletaexam->boletaexamSolicitados=$rqPaci->get('boletaexamSolicitados');
            $boletaexam->save();
            return redirect('/boletaexam/'.$boletaexam->id);
        }
        else{
            return view('/');
        }
        
    }
//actualizar
public function edit(Request $request, $id)
    {
        //
        $boletaexam=Boletaexam::findorFail($id);          
        //dd($boletaexam->boletaexamSolicitados);  
        return view('boletaexam/boletaexamEdit',compact('boletaexam'));
    }
    public function update(Request $request, BoletaexamRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $boletaexam= Boletaexam::findOrFail($id);
            $boletaexam->boletaexamSolicitados=$parq->get('boletaexamSolicitados');
            $boletaexam->update();
            return redirect('/boletaexam/'.$boletaexam->id);
        }
        else{
            return view('/');
        }
         }

         //eliminar
public function destroy(Request $request, $id){
    //dd($id);
    if($request->user()->usuarioTipo=='0'){
        Boletaexam::destroy($id);
        Session::flash('status','¡Boleta de examen, eliminada con exito.');
        return redirect()->action('boletaexamController@index');
       }
       else{
        return view('/');
       }
    }
    
}
