<?php

namespace App\Http\Controllers;
use App\Clinica;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    #Requeri logueo siempre
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    #Renderizacion de vista inicial
    public function index(Request $request)
    {
        //Este parametro es para la vista heredada.

        return view('home');
    }
}
