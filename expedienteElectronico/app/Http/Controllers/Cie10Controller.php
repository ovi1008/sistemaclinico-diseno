<?php

namespace App\Http\Controllers;

use App\Cie10;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Cie10Request;
use DB;
use Session; 

class Cie10Controller extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$cie10 = DB::table('cie10')->where('cie10Sexo', '=', "1")->get();
        $cie10 = DB::table('cie10')->paginate(20);
        //dd($cie10);
    	return view('cie10/cie10List',compact('cie10'));
        }
        else{
            return view('/');

        }
        
    }

    public function show(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
        $cie10 = Cie10::findorFail($id);
        //dd($cie10);
        return view('cie10/cie10Detalle',compact('cie10'));
        }
        else{
            return view('/');
        }
    }

    public function create(Request $request)
    {       //
        if($request->user()->usuarioTipo=='0'){
            return view('cie10/cie10New'); 
        }
        else{
            return view('/');
        }
    }

    public function store(Request $request,cie10Request $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $cie10= new Cie10;
            $cie10->cie10Codigo=$rqPaci->get('cie10Codigo');
            $cie10->cie10Descripcion=strtoupper($rqPaci->get('cie10Descripcion'));
            $cie10->cie10LimiteInferior=$rqPaci->get('cie10LimiteInferior');
            $cie10->cie10LimiteSuperior=$rqPaci->get('cie10LimiteSuperior');
            $cie10->cie10Sexo=$rqPaci->get('cie10Sexo');

            $cie10->save();
            return redirect('/cie10/'.$cie10->id);
        }
        else{
            return view('/');
        }
        
    }

    public function edit(Request $request, Cie10 $cie10)
    {
        //          
        //dd($cie10);  
            return view('cie10/cie10Edit',compact('cie10'));
    }

    public function update(Request $request, cie10Request $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $cie10= Cie10::findOrFail($id);
            $cie10->cie10Codigo=$parq->get('cie10Codigo');
            $cie10->cie10Descripcion=strtoupper($parq->get('cie10Descripcion'));
            $cie10->cie10LimiteSuperior=$parq->get('cie10LimiteSuperior');
            $cie10->cie10LimiteInferior=$parq->get('cie10LimiteInferior');
            $cie10->cie10Sexo=$parq->get('cie10Sexo');                     
            $cie10->update();
            return redirect('/cie10/'.$cie10->id);
        }
        else{
            return view('/');
        }
         }

    public function destroy(Request $request, $id){
           //dd($id);
           if($request->user()->usuarioTipo=='0'){
            Cie10::destroy($id);
            Session::flash('status','¡Enfermedad, eliminada con exito del catalogo CIE10.');
            return redirect()->action('Cie10Controller@index');
           }
           else{
            return view('/');
           }
             
      }

}
