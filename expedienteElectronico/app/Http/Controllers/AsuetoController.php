<?php

namespace App\Http\Controllers;

use App\Asueto;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AsuetoRequest;
use DB;
use Session;
use App\User;

class AsuetoController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$asueto = DB::table('asueto')->where('asuetoSexo', '=', "1")->get();
        $asueto = DB::table('asueto')->get();
        //dd($asueto);
    	return view('asueto/asuetoList',compact('asueto'));
        }
        else{
            return view('/');

        }
        
    }

    //show
    public function show(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
        $asueto = Asueto::findorFail($id);
        //dd($asueto);
        return view('asueto/asuetoDetalle',compact('asueto'));
        }
        else{
            return view('/');
        }
    }

    //llama la vista de crear
    public function create(Request $request)
    {       //
        if($request->user()->usuarioTipo=='0'){
            return view('asueto/asuetoNew'); 
        }
        else{
            return view('/');
        }
    }

    public function store(Request $request,AsuetoRequest $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $asueto= new Asueto;
            $asueto->asuetoNombreDia=$rqPaci->get('asuetoNombreDia');
            $asueto->asuetoFecha=$rqPaci->get('asuetoFecha');
            $asueto->save();
            return redirect('/asueto/'.$asueto->id);
        }
        else{
            return view('/');
        }
        
    }

    public function edit(Request $request, Asueto $asueto)
    {
        //            
            return view('asueto/asuetoEdit',compact('asueto'));
    }

    public function update(Request $request, AsuetoRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $asueto= Asueto::findOrFail($id);
            $asueto->asuetoNombreDia=$parq->get('asuetoNombreDia');
            $asueto->asuetoFecha=$parq->get('asuetoFecha');
            $asueto->update();
            return redirect('/asueto/'.$asueto->id);
        }
        else{
            return view('/');
        }
         }


         public function destroy(Request $request, $id){
            //dd($id);
            if($request->user()->usuarioTipo=='0'){
             Asueto::destroy($id);
             Session::flash('status','¡Dia festivo, eliminada con exito.');
             return redirect()->action('AsuetoController@index');
            }
            else{
             return view('/');
            }
              
       }

}
