<?php

namespace App\Http\Controllers;

use App\Consulta;
use Illuminate\Http\Request;
use App\Http\Requests\ConsultaRequest;
use App\Preparacion;
use App\Paciente;
use App\User;
use App\Especialidad;
use Carbon\Carbon;
use DB;

class ConsultaController extends Controller
{
    //
    public function __construct()
    {
		$this->middleware('auth');        
    }

    public function listarC(Request $request,$id){
        if($request->user()->usuarioTipo=='3'){
            return view('/');

        }else{
            //
            $consulta="SELECT users.id,users.usuarioNombre, users.usuarioApellido, especialidades.especialidadNombre
            FROM users,especialidades
            WHERE users.idEspecialidad=especialidades.id AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            $signos = DB::table('preparaciones')->where('preparacionAsignacion','=',$id)->where('preparacionEstado','=','1')->get();
            //$doctores = DB::table('users')->where('usuarioTipo','=','0')->orWhere('usuarioTipo','=','2')->get();
            $doctores=DB::select( DB::raw($consulta));
            $pacientes=DB::table('pacientes')->get();
            return view('consulta/consultaList',compact('signos','doctores','id','pacientes'));
            //dd($doctor);
        }

    }

    public function crearC(Request $request,$id){

        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='2')){
            $signo=Preparacion::findOrFail($id);
            $paciente=Paciente::findOrFail($signo->idPaciente);
            $fecha = explode("/", $paciente->pacienteFechaNacimiento);
            $edad=Carbon::createFromDate($fecha[2], $fecha[1], $fecha[0])->age;
            $fechaA = Carbon::now();
            $fechaA=$fechaA->format('d/m/Y H:i:s');
            return view('consulta/consultaNew',compact('signo', 'id', 'paciente','edad', 'fechaA'));            
        }else{
            return redirect()->action('PacienteController@index');

        }

    }

    public function obtenerEnfermedades(Request $request, $nombre){
        if($request->ajax()){
            $cie10 = DB::table('cie10')->where('cie10Descripcion', 'like', strtoupper($nombre))->get();
            return response()->json($cie10);
        }

    }

    public function store(Request $request,ConsultaRequest $rqCon)
    {
        //
        if(($request->user()->usuarioTipo=='0')||($request->user()->usuarioTipo=='2')){
            $consulta= new Consulta;
            $consulta->idPacientePreparacion=$rqCon->get('idPacientePreparacion');
            $consulta->consultaConsultaPor=$rqCon->get('consultaConsultaPor');
            $consulta->consultaPresentaEnfermedad=$rqCon->get('consultaPresentaEnfermedad');
            $consulta->consultaAntecedentesPersonales=$rqCon->get('consultaAntecedentesPersonales');
            $consulta->consultaAntecedentesFamiliares=$rqCon->get('consultaAntecedentesFamiliares');
            $consulta->consultaExploracionClinica=$rqCon->get('consultaExploracionClinica');
            $consulta->consultaDiagnosticoPrincipal=$rqCon->get('consultaDiagnosticoPrincipal');
            $consulta->consultaOtrosDiagnosticos=$rqCon->get('consultaOtrosDiagnosticos');
            $consulta->consultaTratamiento=$rqCon->get('consultaTratamiento');
            $consulta->consultaObservaciones=$rqCon->get('consultaObservaciones');
            $consulta->consultaEnfermedadP=$rqCon->get('consultaEnfermedadP');
            $consulta->consultaEnfermedadS1=$rqCon->get('consultaEnfermedadS1');
            $consulta->consultaEnfermedadS2=$rqCon->get('consultaEnfermedadS2');
            $consulta->save();
            $sigAc=Preparacion::findOrFail($consulta->idPacientePreparacion);
            $sigAc->preparacionEstado='0';
            $sigAc->update();
            return redirect('/consulta/'.$consulta->idPacientePreparacion);
        }else{
            return redirect()->action('PacienteController@index');
        }
    }

    public function show(Request $request,$id){
        if(($request->user()->usuarioTipo=='0')||$request->user()->usuarioTipo=='2'){
            $consulta = Consulta::where('idPacientePreparacion',$id)->firstOrFail();
            $signo = Preparacion::findorFail($consulta->idPacientePreparacion);
            $paciente = Paciente::findorFail($signo->idPaciente);
            $doctor = User::findorFail($signo->preparacionAsignacion);
            $espe = Especialidad::findorFail($doctor->idEspecialidad);
            return view('consulta/consultaDetalle',compact('consulta','signo','paciente','doctor','espe'));
        }else{
            return redirect()->action('PacienteController@index');
        }

    }

}
