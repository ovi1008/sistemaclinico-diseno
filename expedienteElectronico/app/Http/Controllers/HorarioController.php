<?php

namespace App\Http\Controllers;

use App\Horario;
use App\Http\Requests\HorarioRequest;
use App\User;
use Illuminate\Http\Request;
use DB;
use Session; 

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $consulta="SELECT horarios.id, users.usuarioNombre, users.usuarioApellido FROM horarios INNER JOIN users ON horarios.horarioDoctorId=users.id";
            $doctores=DB::select( DB::raw($consulta));
            return view('horario/horarioList',compact('doctores'));
        }
        else{
            return view('/');
           }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $consulta="SELECT users.id, users.usuarioNombre, users.usuarioNombre2, users.usuarioApellido, users.usuarioApellido2 FROM users WHERE (users.id NOT IN (SELECT horarios.horarioDoctorId FROM horarios)) AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            $doctores=DB::select( DB::raw($consulta));
            return view('horario/horarioNew',compact('doctores')); 
        }
        else{
            return view('/');
           }
            
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, HorarioRequest $rqHorario)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $horario = new Horario;
            $horario->horarioDoctorId=$rqHorario->get('horarioDoctorId');
            $horario->horarioHoraI=$rqHorario->get('horarioHoraI');
            $horario->horarioHoraF=$rqHorario->get('horarioHoraF');
            $horario->horarioPaciente=$rqHorario->get('horarioPaciente');
            $horario->save();
            return redirect('/horarios/'.$horario->id);
        }
        else{
            return view('/');
           }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $horario = Horario::findorFail($id);
            $doctor = User::findorFail($horario->horarioDoctorId);
            return view('horario/horarioDetalle',compact('horario','doctor'));
        }
        else{
            return view('/');
           }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Horario $horario)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $consulta="SELECT users.id, users.usuarioNombre, users.usuarioNombre2, users.usuarioApellido, users.usuarioApellido2 FROM users WHERE (users.id NOT IN (SELECT horarios.horarioDoctorId FROM horarios)) AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            $doctores=DB::select( DB::raw($consulta));
            return view('horario/horarioEdit',compact('horario','doctores'));
        }
        else{
            return view('/');
           }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, HorarioRequest $rqHorario)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $horario = Horario::findorFail($id);
            $horario->horarioHoraI=$rqHorario->get('horarioHoraI');
            $horario->horarioHoraF=$rqHorario->get('horarioHoraF');
            $horario->horarioPaciente=$rqHorario->get('horarioPaciente');
            $horario->update();
            return redirect('/horarios/'.$horario->id);
        }
        else{
            return view('/');
           }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $accion= Horario::where('id','=',$id)->delete();
            Session::flash('status','¡Horario eliminado con exito.');
            return redirect()->action('HorarioController@index');
        }
        else{
            return view('/');
           }
    }
}
