<?php

namespace App\Http\Controllers;

use App\Referencia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ReferenciaRequest;
use DB;
use Session;
use App\User;

class ReferenciaController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$referencia = DB::table('referencia')->where('referenciaSexo', '=', "1")->get();
        $referencia = DB::table('referencia')->get();
        //dd($referencia);
    	return view('referencia/referenciaList',compact('referencia'));
        }
        else{
            return view('/');
        }    
    }

    //show
public function show(Request $request, $id)
{
    //
    if($request->user()->usuarioTipo=='0'){
    $referencia = Referencia::findorFail($id);
    //dd($referencia);
    return view('referencia/referenciaDetalle',compact('referencia'));
    }
    else{
        return view('/');
    }
}


//llama la vista de crear
public function create(Request $request)
{
    if($request->user()->usuarioTipo=='0'){
    return view('referencia/referenciaNew'); 
}
else{
    return view('/');
}
}

public function store(Request $request,ReferenciaRequest $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $referencia= new Referencia;
            $referencia->referenciaMotivos=$rqPaci->get('referenciaMotivos');
            $referencia->referenciaObservaciones=$rqPaci->get('referenciaObservaciones');
            $referencia->save();
            return redirect('/referencia/'.$referencia->id);
        }
        else{
            return view('/');
        }
        
    }

    //actualizar
public function edit(Request $request, $id)
{
    //
    $referencia=Referencia::findorFail($id);          
    //dd($referencia->referenciaMotivos);  
    return view('referencia/referenciaEdit',compact('referencia'));
}
public function update(Request $request, ReferenciaRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $referencia= Referencia::findOrFail($id);
            $referencia->referenciaMotivos=$parq->get('referenciaMotivos');
            $referencia->referenciaObservaciones=$parq->get('referenciaObservaciones');
            $referencia->update();
            return redirect('/referencia/'.$referencia->id);
        }
        else{
            return view('/');
        }
        }

//eliminar
public function destroy(Request $request, $id){
    //dd($id);
    if($request->user()->usuarioTipo=='0'){
        Referencia::destroy($id);
        Session::flash('status','¡Referencia, eliminada con exito.');
        return redirect()->action('ReferenciaController@index');
       }
       else{
        return view('/');
       }
    }
    
}
