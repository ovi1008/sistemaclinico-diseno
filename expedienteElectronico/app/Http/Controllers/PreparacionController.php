<?php

namespace App\Http\Controllers;

use App\Especialidad;
use Illuminate\Http\Request;
use App\Http\Requests\ProcedimientoRequest;
use App\Paciente;
use DB;
use App\Preparacion;
use App\User;
use Session; 
use Carbon\Carbon;

class PreparacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
		$this->middleware('auth');        
    }

    public function listarSignos(Request $request,$id){
        if($request->user()->usuarioTipo=='3'){
            return view('/');

        }else{
            //
            $consulta="SELECT users.id,users.usuarioNombre, users.usuarioApellido, especialidades.especialidadNombre
            FROM users,especialidades
            WHERE users.idEspecialidad=especialidades.id AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            $signos = DB::table('preparaciones')->where('idPaciente','=',$id)->get();
            //$doctores = DB::table('users')->where('usuarioTipo','=','0')->orWhere('usuarioTipo','=','2')->get();
            $doctores=DB::select( DB::raw($consulta));
            $pacienteA=Paciente::findorFail($id);
            return view('signos/signosList',compact('signos','doctores','id','pacienteA'));
            //dd($doctor);
        }

    }

    public function index(Request $request)
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearSig(Request $request,$id)
    {
        //
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $consulta="SELECT users.id,users.usuarioNombre, users.usuarioApellido, especialidades.especialidadNombre
            FROM users,especialidades
            WHERE users.idEspecialidad=especialidades.id AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            
            $doctores = DB::select( DB::raw($consulta));
            $paciente=Paciente::findOrFail($id);
            $fecha = explode("/", $paciente->pacienteFechaNacimiento);
            $edad=Carbon::createFromDate($fecha[2], $fecha[1], $fecha[0])->age;
            $fechaA = Carbon::now();
            $fechaA=$fechaA->format('d/m/Y H:m:s');
            //('l jS \\of F Y h:i:s A');
    		return view('signos/signosNew',compact('doctores', 'id', 'paciente','edad', 'fechaA'));

        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,ProcedimientoRequest $rqSig)
    {
        //
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $signo= new Preparacion;
            $signo->idPaciente=$rqSig->get('idPaciente');
            $signo->preparacionPulso=$rqSig->get('preparacionPulso');
            $signo->preparacionPeso=$rqSig->get('preparacionPeso');
            $signo->preparacionTemperatura=$rqSig->get('preparacionTemperatura');
            $signo->preparacionAltura=$rqSig->get('preparacionAltura');
            $signo->preparacionAsignacion=$rqSig->get('preparacionAsignacion');
            $signo->save();
            return redirect('/preparacion/'.$signo->id);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editSig(Request $request, $id)
    {
        //
        $signo=Preparacion::findOrFail($id);
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $consulta="SELECT users.id,users.usuarioNombre, users.usuarioApellido, especialidades.especialidadNombre
            FROM users,especialidades
            WHERE users.idEspecialidad=especialidades.id AND (users.usuarioTipo='0' OR users.usuarioTipo='2')";
            
            $doctores = DB::select( DB::raw($consulta));
            if($signo->preparacionEstado==0){
                return redirect('/preparacion/'.$signo->id);
            }
            //dd($signo);
    		return view('signos/signosEdit',compact('signo', 'doctores'));

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,ProcedimientoRequest $rqSig,$id)
    {
        //
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $signo= Preparacion::findOrFail($id);
            if($signo->preparacionEstado==0){
                return redirect('/preparacion/'.$signo->id);
            }
            $signo->preparacionPulso=$rqSig->get('preparacionPulso');
            $signo->preparacionPeso=$rqSig->get('preparacionPeso');
            $signo->preparacionTemperatura=$rqSig->get('preparacionTemperatura');
            $signo->preparacionAltura=$rqSig->get('preparacionAltura');
            $signo->preparacionAsignacion=$rqSig->get('preparacionAsignacion');
            $signo->update();
            return redirect('/preparacion/'.$signo->id);
        }
    }

    public function show(Request $request,$id){
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $signo = Preparacion::findorFail($id);
            $paciente = Paciente::findorFail($signo->idPaciente);
            $doctor = User::findorFail($signo->preparacionAsignacion);
            $espe = Especialidad::findorFail($doctor->idEspecialidad);
            return view('signos/signosDetalle',compact('signo','paciente','doctor','espe'));

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='3'){
            return redirect()->action('PacienteController@index');
        }else{
            $signo = Preparacion::findOrFail($id);
            $idP = $signo->idPaciente;
            if($signo->preparacionEstado=='1'){
                $accion= Preparacion::where('id','=',$id)->delete();
                Session::flash('status','¡Datos de signos vitales, eliminados con exito.');
                return redirect('/preparacion/listado/'.$idP);
            }
            Session::flash('status','¡Ya se completo la consulta, prohibido eliminar!');
            return redirect('/preparacion/listado/'.$idP);
        }
    }
}
