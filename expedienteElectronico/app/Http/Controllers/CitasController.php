<?php

namespace App\Http\Controllers;

use App\Citas;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CitasRequest;
use DB;
use Session;
use App\User;

class CitasController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            //$citas = DB::table('citas')->where('citasSexo', '=', "1")->get();
        $citas = DB::table('citas')->get();
        //dd($citas);
    	return view('citas/citasList',compact('citas'));
        }
        else{
            return view('/');
            }
        }

    //show
    public function show(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
        $citas = Citas::findorFail($id);
        //dd($citas);
        return view('citas/citasDetalle',compact('citas'));
        }
        else{
            return view('/');
        }
    }

    //llama la vista de crear
    public function create(Request $request)
    {       
        if($request->user()->usuarioTipo=='0'){
            return view('citas/citasNew'); 
        }
        else{
            return view('/');
        }
    }

    public function store(Request $request,CitasRequest $rqPaci)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $citas= new Citas;
            $citas->idDoctor=$rqPaci->get('idDoctor');
            $citas->idPaciente=$rqPaci->get('idPaciente');
            $citas->fechaCita=$rqPaci->get('fechaCita');
            $citas->horaCita=$rqPaci->get('horaCita');
            $citas->save();
            return redirect('/citas/'.$citas->id);
        }
        else{
            return view('/');
        }
        
    }

    //llama la vista ditar
    public function edit(Request $request, Citas $citas, $id)
    {           
        $citas=Citas::findorFail($id);          
        return view('citas/citasEdit',compact('citas'));
    }

    public function update(Request $request, CitasRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $citas= Citas::findOrFail($id);
            $citas->idDoctor=$parq->get('idDoctor');
            $citas->idPaciente=$parq->get('idPaciente');
            $citas->fechaCita=$parq->get('fechaCita');
            $citas->horaCita=$parq->get('horaCita');
            $citas->update();
            return redirect('/citas/'.$citas->id);
        }
        else{
            return view('/');
        }
        }

    //destroy
    public function destroy(Request $request, $id){
        //dd($id);
        if($request->user()->usuarioTipo=='0'){
         Citas::destroy($id);
         Session::flash('status','Cita, eliminada con exito.');
         return redirect()->action('CitasController@index');
        }
        else{
         return view('/');
        }
          
   }
}
