<?php

namespace App\Http\Controllers;

use App\User;
use App\Clinica;
use App\Http\Requests\ClinicaRequest;
use Illuminate\Http\Request;

use Carbon\Carbon;

class ClinicaController extends Controller
{
    //
    public function __construct()
    {
		$this->middleware('auth');        
    }

    public function index(Request $request){
		$clinica = Clinica::findOrFail(1);
		 //Este parametro es para la vista heredada.
		 return view('clinica/clinicaDetalle',compact('clinica'));

    }

    public function edit(Request $request ,Clinica $clinica)
	{
		if($request->user()->usuarioTipo  == '0'){
			//dd($clinica->nombre);
        return view('clinica/clinicaEditar',['clinica'=>$clinica]);
    	}
    	return redirect('/');
	}
    
    public function update(ClinicaRequest $request,$id)
	{
		$clinica=Clinica::findOrFail($id);

		if($file=$request->file('clinicaImagen')){
			$old_image=$clinica->clinicaImagen;
			if(strcmp($old_image,'clinica_default01.jpg')){
				if(is_file(public_path().'/img/clinica/'.$old_image)){
					unlink(public_path().'/img/clinica/'.$old_image);
				} 
			}
			$file->move(public_path().'/img/clinica',Carbon::now()->second.'_'.$file->getClientOriginalName());
			$clinica->clinicaImagen=Carbon::now()->second.'_'.$file->getClientOriginalName();
		}
		if($file=$request->file('clinicaLogo')){
			$old_image=$clinica->clinicaLogo;
			if(strcmp($old_image,'logoClinica.jpg')){
				if(is_file(public_path().'/img/clinica/'.$old_image)){
					unlink(public_path().'/img/clinica/'.$old_image);
				} 
			}
			$file->move(public_path().'/img/clinica',Carbon::now()->second.'_'.$file->getClientOriginalName());
			$clinica->clinicaLogo=Carbon::now()->second.'_'.$file->getClientOriginalName();
		}
		$clinica->clinicaNombre=$request->get('clinicaNombre');
		$clinica->clinicaDescripcion=$request->get('clinicaDescripcion');
		$clinica->clinicaDireccion=$request->get('clinicaDireccion');
		$clinica->clinicaTelefono=$request->get('clinicaTelefono');
        $clinica->update();


		return redirect()->action('ClinicaController@index');
	}
}
