<?php

namespace App\Http\Controllers;

use App\Especialidad;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\EspecialidadRequest;
use DB;
use Session; 

class EspecialidadController extends Controller
{
    public function __construct()
    {
		$this->middleware('auth');        
    }


    public function index(Request $request)
    {
        //
        if($request->user()->usuarioTipo=='0'){
        $especialidades = DB::table('especialidades')->get();;
    	return view('especialidad/especialidadList',compact('especialidades'));
        }
        else{
            return view('/');

        }
        
    }

    public function show(Request $request, $id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
        $especialidades = Especialidad::findorFail($id);
        return view('especialidad/especialidadDetalle',compact('especialidades'));
        }
        else{
            return view('/');
        }
    }

    public function create(Request $request)
    {       //
        if($request->user()->usuarioTipo=='0'){
            return view('especialidad/especialidadNew'); 
        }
        else{
            return view('/');
        }
    }

    public function store(Request $request,especialidadRequest $rqEspe)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $especialidad= new Especialidad;
            $especialidad->especialidadNombre=$rqEspe->get('especialidadNombre');

            $especialidad->save();
            return redirect('/especialidad/'.$especialidad->id);
        }
        else{
            return view('/');
        }
        
    }

    public function edit(Request $request, Especialidad $especialidad)
    {
        return view('especialidad/especialidadEdit',compact('especialidad'));
    }

    public function update(Request $request, especialidadRequest $parq,$id)
    {
        //
        if($request->user()->usuarioTipo=='0'){
            $especialidad= Especialidad::findOrFail($id);
            $especialidad->especialidadNombre=$parq->get('especialidadNombre');
            $especialidad->update();
            return redirect('/especialidad/'.$especialidad->id);
        }
        else{
            return view('/');
        }
         }



    public function destroy(Request $request, $id){
           //dd($id);
           if($request->user()->usuarioTipo=='0'){
            especialidad::destroy($id);
            Session::flash('status','Especialidad, eliminada con exito.');
            return redirect()->action('EspecialidadController@index');
           }
           else{
            return view('/');
           }
             
      }


}
