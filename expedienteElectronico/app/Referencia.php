<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referencia extends Model
{
    protected $table='referencia';
    protected $fillable = [
        'referenciaMotivos', 'referenciaObservaciones',  
    ];

    public $timestamps = false;

}
