@extends('admin.template')

@section('tituloTab')
CIE10   
@endsection

@section('tituloP')
Detalle de la enfermedad segun CIE10
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('Cie10Controller@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Codigo: </td>
                        <td> {{$cie10->cie10Codigo}}</td>
                </tr>
                        
                    <tr>
                        <td >Descripcion: </td>
                        <td> {{$cie10->cie10Descripcion}}</td>
                    </tr>
                    <tr>
                                <td >Sexo: </td>
                                <td>
                                                @if(($cie10->cie10Sexo)=="0")
                                                <span class="label label-info">Ambos sexos</span> </td>
                                                @elseif(($cie10->cie10Sexo)=="1")
                                                <span class="label label-danger">Masculino</span>
                                                @elseif(($cie10->cie10Sexo)=="2")
                                                <span class="label label-danger">Femenino</span>
                                                @endif
                                </td>
                        </tr>
                    <tr>
                            <td >Limite Inferior de edad: </td>
                            <td> {{$cie10->cie10LimiteInferior}}</td>
                    </tr>
                    <tr>
                            <td >Limite Superior de edad: </td>
                            <td> {{$cie10->cie10LimiteSuperior}}</td>
                    </tr>
                    
                    
                        
                </tbody>
            </table>

@endsection
