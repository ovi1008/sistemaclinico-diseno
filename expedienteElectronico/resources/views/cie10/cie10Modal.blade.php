<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$cie10A->id}}">
	{{Form::Open(array('action'=>array('Cie10Controller@destroy',$cie10A->id),'method'=>'delete'))}}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title">Eliminar enfermedad del catalogo</h4>
				</div>
				<div class="modal-body">
					<b>¿Desea eliminar la enfermedad con el codigo {{$cie10A->cie10Codigo}} y descripcion {{$cie10A->cie10Descripcion}}?</b> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger">Confirmar</button>
				</div>
			</div>
		</div>
	{{Form::Close()}}
</div>