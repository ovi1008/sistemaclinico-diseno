@extends('admin.template')

@section('tituloTab')
CIE10  
@endsection

@section('tituloP')
Editar Diagnostico para CIE10
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($cie10,['method'=>'PATCH','route'=>['cie10.update',$cie10->id]])!!}
{{Form::token()}}  

            
                <div class=" form-group{{ $errors->has('cie10Codigo') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Codigo</label>
                        <input id="cie10Codigo" type="text" class="form-control" name="cie10Codigo" placeholder="codigo" value="{{$cie10->cie10Codigo}}" required autocomplete="cie10Codigo" autofocus onkeypress="return  onpaste="return false" maxlength="10">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('cie10Codigo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
                <div class=" form-group{{ $errors->has('cie10Descripcion') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Descripcion</label>
                        <input id="cie10Descripcion" type="text" class="form-control" name="cie10Descripcion" placeholder="Descripcion" value="{{$cie10->cie10Descripcion}}"  autocomplete="cie10Descripcion" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="150">

                                <div class="help-block with-errors">
                                        @error('cie10Descripcion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                </div>

            <div class="form-group{{ $errors->has('cie10Sexo') ? ' has-error' : '' }}">
                    <label class="control-label">Sexo del cie10</label>
                        <select name="cie10Sexo" id="cie10Sexo"class="form-control" onchange="ShowSelected();"required>
                            <option value="">---Seleccion el sexo---</option>
                            @if ($cie10->cie10Sexo == "0")
                            <option value="0" selected>Ambos Sexos</option>                            
                            @else
                            <option value="0">Ambos Sexos</option>                                
                            @endif
                            @if ($cie10->cie10Sexo=="1")
                            <option value="1" selected>Masculino</option>                            
                            @else
                            <option value="1">Masculino</option>                                
                            @endif
                            @if ($cie10->cie10Sexo=="2")
                            <option value="2" selected>Femenino</option>                            
                            @else
                            <option value="2">Femenino</option>                                
                            @endif
                        </select>
                        <div class="help-block with-errors">
                                @error('cie10Sexo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
        
                </div>
                
                    <div class="form-group {{ $errors->has('cie10LimiteInferior') ? ' has-error' : '' }}">
                            <label class="control-label">Limite inferior de edad:</label>
                            <div class="input-group">
                                    <input id="cie10LimiteInferior" type="text" class="form-control" name="cie10LimiteInferior" value="{{$cie10->cie10LimiteInferior}}"  autocomplete="cie10LimiteInferior" onkeypress="return soloNumeros(event)" onpaste="return false" maxlength="3">                       
                                <div class="help-block with-errors">
                                        @error('cie10LimiteInferior')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-4">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                        </div>

                        <div class="form-group {{ $errors->has('cie10LimiteSuperior') ? ' has-error' : '' }}">
                            <label  class="control-label">Limite Superior de edad:</label>
                            <div class="input-group">
                                    <input id="cie10LimiteSuperior" type="text" class="form-control" name="cie10LimiteSuperior" value="{{$cie10->cie10LimiteSuperior}}"  autocomplete="cie10LimiteSuperior" onkeypress="return soloNumeros(event)" onpaste="return false" maxlength="3">                       
                                <div class="help-block with-errors">
                                        @error('cie10LimiteSuperior')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-4">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                        </div>

                        

                           
        
  
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#cie10Departamento").change(event => {
	$.get("cie10/departamento/"+event.target.value+"", function(res, sta){
		$("#cie10Municipio").empty();
		res.forEach(element => {
			$("#cie10Municipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
