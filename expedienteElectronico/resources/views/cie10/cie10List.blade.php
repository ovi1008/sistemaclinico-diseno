@extends('admin.template')

@section('tituloTab')
Cie10  
@endsection

@section('tituloP')
Lista de CIE10
@endsection


@section('contenido')
	  
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<div class="scrollable">
				<div class="table-responsive">
					<!-- <table id="tablacie10" class="table m-t-30 table-hover contact-list" data-page-size="10"> -->
					<table  class="table m-t-30 table-hover contact-list" >
						<thead>
								<th>Codigo </th>
						
								<th>Descripcion</th>
								<th>sexo</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($cie10 as $cie10A)
							<tr>
								<td>
								<a href="{{URL::action('Cie10Controller@show',$cie10A->id)}}">{{$cie10A->cie10Codigo }}</a>
								</td>
			
								<td>{{$cie10A->cie10Descripcion}}</td>									
								<td>
								@if(($cie10A->cie10Sexo)== "0")
								<p>Ambos Sexos</p>
        						@endif
								@if(($cie10A->cie10Sexo)== "1")
								<p>Hombres</p>
        						@endif
								@if(($cie10A->cie10Sexo)== "2")
								<p>Mujeres</p>
        						@endif
								
									</td>
									<td>
									    <a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('Cie10Controller@edit',$cie10A->id)}}"><i class="ti-pencil-alt"></i></a>
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$cie10A->id}}" data-toggle="modal"><i class="icon-trash"></i></a>
									</td>

									
								</tr>
							
							@include('cie10.cie10Modal')									
							@endforeach
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									
									<a href="{{URL::action('Cie10Controller@create')}}">
									<button  type="submit" class="btn btn-info btn-rounded" >Añadir Diagnostico a CIE10</button>
									</a>	
									
								</td>
							</tr>
						</tfoot>
					</table>
					{{ $cie10->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javaPersonalizado')
<script>
		$(document).ready(function() {
			$('#tablacie10').DataTable();
			$(document).ready(function() {
				var table = $('#example').DataTable({
					"columnDefs": [{
						"visible": false,
						"targets": 2
					}],
					"order": [
						[2, 'asc']
					],
					"displayLength": 25,
					"drawCallback": function(settings) {
						var api = this.api();
						var rows = api.rows({
							page: 'current'
						}).nodes();
						var last = null;
						api.column(2, {
							page: 'current'
						}).data().each(function(group, i) {
							if (last !== group) {
								$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
								last = group;
							}
						});
					}
				});
				// Order by the grouping
				$('#example tbody').on('click', 'tr.group', function() {
					var currentOrder = table.order()[0];
					if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
						table.order([2, 'desc']).draw();
					} else {
						table.order([2, 'asc']).draw();
					}
				});
			});
		});
		$('#example23').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
		$('#tablacie10').DataTable( {
			"lengthMenu": [ [20, 40, 60, -1], [20, 40, 60, "All"] ],
  			 "pageLength": 20,
			   "aaSorting": [],
			   "aoColumns": [ null, null, null, { "bSortable": false } ],
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
	

		</script>	
@endsection