@extends('admin.template')

@section('tituloTab')
CITAS  
@endsection

@section('tituloP')
REGISTRAR CITAS
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::open(array('url'=>'/citas','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}  
                <div class=" form-group{{ $errors->has('idDoctor') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre del Doctor</label>
                        <input id="idDoctor" type="text" class="form-control" name="idDoctor" placeholder="Doctor" value="{{ old('idDoctor') }}" required autocomplete="idDoctor" autofocus  onpaste="return false" maxlength="10">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('idDoctor')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                <div class=" form-group{{ $errors->has('idPaciente') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre del Paciente</label>
                        <input  id="idPaciente" type="text" class="form-control" name="idPaciente" placeholder="Nombre de Paciente" value="{{ old('idPaciente') }}"  autocomplete="idPaciente" autofocus  maxlength="10">

                                <div class="help-block with-errors">
                                        @error('idPaciente')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>
                <div class=" form-group{{ $errors->has('fechaCita') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Fecha de la cita</label>
                        <input data-mask="99/99/9999" id="fechaCita" type="text" class="form-control" name="fechaCita" placeholder="Fecha de la cita DD/MM/AAAA" value="{{ old('fechaCita') }}"  autocomplete="fechaCita" autofocus  title="12/12/2019">

                                <div class="help-block with-errors">
                                        @error('fechaCita')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>
                <div class=" form-group{{ $errors->has('horaCita') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Hora de la cita</label>
                        <input data-mask="99:99" id="horaCita" type="text" class="form-control" name="horaCita" placeholder="Hora de la cita HH:MM" value="{{ old('horaCita') }}"  autocomplete="horaCita" autofocus  title="12:12">

                                <div class="help-block with-errors">
                                        @error('horaCita')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#citasDepartamento").change(event => {
	$.get("citas/departamento/"+event.target.value+"", function(res, sta){
		$("#citasMunicipio").empty();
		res.forEach(element => {
			$("#citasMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
