@extends('admin.template')

@section('tituloTab')
CITAS   
@endsection

@section('tituloP')
Detalle de citas
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('CitasController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Doctor que realizara la consulta: </td>
                        <td> {{$citas->idDoctor}}</td>
                </tr>
                <tr>
                        <td >Paciente que reservo la cita: </td>
                        <td> {{$citas->idPaciente}}</td>
                </tr>
                        
                    <tr>
                        <td >Fecha de la cita: </td>
                        <td> {{$citas->fechaCita}}</td>
                    </tr>
                    <tr>
                            <td >Hora de la cita: </td>
                            <td> {{$citas->horaCita}}</td>
                    </tr>
                                       
                    
                        
                </tbody>
            </table>

@endsection
