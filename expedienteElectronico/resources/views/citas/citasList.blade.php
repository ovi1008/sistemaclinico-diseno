@extends('admin.template')

@section('tituloTab')
CITAS  
@endsection

@section('tituloP')
Lista de Citas
@endsection


@section('contenido')
	  
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<br><br>
			<div class="scrollable">
				<div class="table-responsive">
				<table id="prueba2" class="display">
						<thead>
                                <th>Doctor</th>
								<th>Paciente</th>
								<th>Hora de la cita</th>
								<th>Fecha de la cita</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($citas as $use)
							<tr>
									<td>
                                    <a href="{{URL::action('CitasController@show',$use->id)}}">{{$use->idDoctor }}
                                    </td>
                                    <td>
                                    {{$use->idPaciente }}</a>
									</td>
									
								<td>{{$use->fechaCita}}</td>
								<td>{{$use->horaCita}}</td>
									
									<td>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('CitasController@edit',$use->id)}}"><i class="ti-pencil-alt"></i></a>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$use->id}}" data-toggle="modal"><i class="icon-trash"></i></a>
									</td>
								</tr>
                                @include('citas.citasModal')		
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									<a href="{{URL::action('CitasController@create')}}">
									<button  type="submit" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Nuevo citas</button>
									</a>
								</td>
							</tr>
						</tfoot>
					</table>


                    

				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javaPersonalizado')
		<script>
		$(document).ready( function () {
			$('#prueba2').DataTable();
			} );
		$('#prueba2').DataTable( {
			"lengthMenu": [ [8, 16, 24, -1], [8, 16, 24, "All"] ],
  			 "pageLength": 8,
			   "aaSorting": [],
			   "aoColumns": [ null, null, { "bSortable": false } ],
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
			</script>	
@endsection


