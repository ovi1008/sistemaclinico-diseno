@extends('admin.template')

@section('tituloTab')
RECETA  
@endsection

@section('tituloP')
Actualizar RECETA
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($receta,['method'=>'PATCH','route'=>['receta.update',$receta->id]])!!}
{{Form::token()}}  
                <div class=" form-group{{ $errors->has('recetaMedicamento') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre de los Medicamentos</label>
                <textarea  id="recetaMedicamento" rows="6"  class="form-control" name="recetaMedicamento" placeholder="Motivo del receta" value="{{$receta->recetaMedicamento}}" required autocomplete="recetaMedicamento" maxlength="250">{{$receta->recetaMedicamento}}</textarea>
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('recetaMedicamento')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                <div class=" form-group{{ $errors->has('recetaObservaciones') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Observaciones</label>
                <textarea  id="recetaObservaciones" rows="6"  class="form-control" name="recetaObservaciones" placeholder="Motivo del receta" value="{{$receta->recetaObservaciones}}" required autocomplete="recetaMedicamento" maxlength="250">{{$receta->recetaObservaciones}}</textarea>                        
                        
                                <div class="help-block with-errors">
                                        @error('recetaObservaciones')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#recetaDepartamento").change(event => {
	$.get("receta/departamento/"+event.target.value+"", function(res, sta){
		$("#recetaMunicipio").empty();
		res.forEach(element => {
			$("#recetaMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
