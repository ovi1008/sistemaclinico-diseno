@extends('admin.template')

@section('tituloTab')
Recetas   
@endsection

@section('tituloP')
Detalle del Receta
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('RecetaController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Medicamento: </td>
                        <td> {{$receta->recetaMedicamento}}</td>
                </tr>
                        
                    <tr>
                        <td >Observaciones: </td>
                        <td> {{$receta->recetaObservaciones}}</td>
                    </tr>
                    
                        
                </tbody>
            </table>

@endsection
