<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$recetaA->id}}">
	{{Form::Open(array('action'=>array('RecetaController@destroy',$recetaA->id),'method'=>'delete'))}}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title">Eliminar Receta</h4>
				</div>
				<div class="modal-body">
					<b>¿Desea eliminar la Receta :{{$recetaA->recetaMedicamento}} ?</b> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger">Confirmar</button>
				</div>
			</div>
		</div>
	{{Form::Close()}}
</div>