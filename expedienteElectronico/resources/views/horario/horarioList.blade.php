@extends('admin.template')

@section('tituloTab')
Asignacion de Horarios
@endsection

@section('tituloP')
Asignacion de Horarios
@endsection


@section('contenido')
	  
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<br><br>
			<div class="scrollable">
				<div class="table-responsive">
					<table id="tablaPaciente" class="table m-t-30 table-hover contact-list" data-page-size="10">
						<thead>
								<th>Id</th>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($doctores as $doc)
							<tr>
								<td>
								<a href="{{URL::action('HorarioController@show',$doc->id)}}">{{$doc->id}}</a>
								</td>
								<td>{{$doc->usuarioNombre}}</td>									
								<td>{{$doc->usuarioApellido}}
								</td>
									<td>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('HorarioController@edit',$doc->id)}}"><i class="ti-pencil-alt"></i></a>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$doc->id}}" data-toggle="modal"><i class="icon-trash"></i></a>
									</td>
								</tr>

							@include('horario.horarioModal')							
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<a href="{{URL::action('HorarioController@create')}}">
									<button  type="submit" class="btn btn-info btn-rounded" >Crear Asignacion Nueva</button>
									</a>
			
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javaPersonalizado')
<script>
		$(document).ready(function() {
			$('#tablaPaciente').DataTable();
			$(document).ready(function() {
				var table = $('#example').DataTable({
					"columnDefs": [{
						"visible": false,
						"targets": 2
					}],
					"order": [
						[2, 'asc']
					],
					"displayLength": 25,
					"drawCallback": function(settings) {
						var api = this.api();
						var rows = api.rows({
							page: 'current'
						}).nodes();
						var last = null;
						api.column(2, {
							page: 'current'
						}).data().each(function(group, i) {
							if (last !== group) {
								$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
								last = group;
							}
						});
					}
				});
				// Order by the grouping
				$('#example tbody').on('click', 'tr.group', function() {
					var currentOrder = table.order()[0];
					if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
						table.order([2, 'desc']).draw();
					} else {
						table.order([2, 'asc']).draw();
					}
				});
			});
		});
		$('#example23').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
		$('#tablaPaciente').DataTable( {
			"lengthMenu": [ [5, 10, 20, -1], [5, 10, 20, "All"] ],
  			 "pageLength": 5,
			   "aaSorting": [],
			   "aoColumns": [ null, null, null, { "bSortable": false } ],
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
	

		</script>	
@endsection