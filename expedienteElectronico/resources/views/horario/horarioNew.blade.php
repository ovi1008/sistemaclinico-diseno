@extends('admin.template')

@section('tituloTab')
Asignación de Horarios
@endsection

@section('tituloP')
Crear de Horario
@endsection

@section('contenido')
<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>


{!!Form::open(array('url'=>'/horarios','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}

<div class="form-group{{ $errors->has('horarioDoctorId') ? ' has-error' : '' }}">
        <label class="control-label">Doctor a asignar horario</label>
            <select name="horarioDoctorId" id="horarioDoctorId"class="form-control" required>
                <option value="">---Seleccion el doctor---</option>
                @foreach ($doctores as $doc)
                <option value="{{$doc->id}}">{{$doc->usuarioNombre}}&nbsp;{{$doc->usuarioNombre2}}&nbsp;{{$doc->usuarioApellido}}&nbsp;{{$doc->usuarioApellido2}}</span></option>  
                @endforeach
            </select>
            <div class="help-block with-errors">
                    @error('horarioDoctorId')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-sm-12">
                    <span class="help-block">
                        <small>
                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                        </small>
                    </span> 
                </div>

    </div>

    <div class="form-group {{ $errors->has('horarioPaciente') ? ' has-error' : '' }}">
            <label for="inputName1" class="control-label">Cantidad de pacientes a atender</label>
            <input id="horarioPaciente" type="text" class="form-control" name="horarioPaciente"  value="{{ old('horarioPaciente') }}"  autocomplete="horarioPaciente" autofocus onkeypress="return soloNumeros(event)" onpaste="return false" maxlength="10">                                    
                    <div class="help-block with-errors">
                            @error('horarioPaciente')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
</div>

    <div class="clockpicker{{ $errors->has('horarioHoraI') ? ' has-error' : '' }}">
                    <label for="inputName1" class="control-label">Hora Inicial</label>
                    <div class="input-group">
                        <input data-mask="99:99" id="horarioHoraI" type="text" class="form-control timepicker" name="horarioHoraI"  value="{{ old('horarioHoraI') }}"  autocomplete="horarioHoraI" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10"><span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                    </div><div class="col-sm-12">
                        </div>    
                                <div class="help-block with-errors">
                                        @error('horarioHoraI')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
        </div>
        <div class="clockpicker form-group{{ $errors->has('horarioHoraF') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Hora Final</label>
                        <div class="input-group">
                         <input data-mask="99:99" id="horarioHoraF" type="text" class="form-control" name="horarioHoraF"  value="{{ old('horarioHoraF') }}"  autocomplete="horarioHoraF" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10"><span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                        </div>
                                 <div class="help-block with-errors">
                                         @error('horarioHoraF')
                                         <span class="invalid-feedback" role="alert">
                                             <strong>{{ $message }}</strong>
                                         </span>
                                     @enderror
                                     </div>
                                     <div class="col-sm-12">
                                             <span class="help-block">
                                                 <small>
                                                         <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                                 </small>
                                             </span> 
                                         </div>
        </div>


   


    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')

<script>
        $('.clockpicker').clockpicker({
        donetext: 'Realizado',
    }).find('input').change(function() {
        console.log(this.value);
    });
</script>
    
@endsection


