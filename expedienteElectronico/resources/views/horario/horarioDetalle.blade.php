@extends('admin.template')

@section('tituloTab')
Asignación de Horarios
@endsection

@section('tituloP')
Detalle de Horario
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('HorarioController@index')}}"><i class="ti-back-left"></i></a>


<h3 class="box-title m-t-40">Informacion Del Horario Del Doctor</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td >Doctor: </td>
                        <td>{{$doctor->usuarioNombre}}&nbsp;{{$doctor->usuarioApellido}}</td>
                    </tr>
                    <tr>
                            <td >Horario Laboral: </td>
                            <td> {{$horario->horarioHoraI}} - {{$horario->horarioHoraF}}</td>
                    </tr>
                    <tr>
                            <td >Numero de paciente a atender: </td>
                            <td>{{$horario->horarioPaciente}}</td>
                    </tr>
                 
                   
                </tbody>
                
            </table>

@endsection
