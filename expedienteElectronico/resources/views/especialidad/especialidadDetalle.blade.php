@extends('admin.template')

@section('tituloTab')
Especialidad   
@endsection

@section('tituloP')
Detalle de especialidad
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('EspecialidadController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Especialidad: </td>
                        <td> {{$especialidades->especialidadNombre}}</td>
                </tr>
                     
                        
                </tbody>
            </table>

@endsection
