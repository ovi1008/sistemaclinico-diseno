@extends('admin.template')

@section('tituloTab')
Especialidad  
@endsection

@section('tituloP')
Crear Especialidad
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::open(array('url'=>'/especialidad','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}  
            
                <div class=" form-group{{ $errors->has('especialidadNombre') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre especialidad</label>
                        <input id="especialidadNombre" type="text" class="form-control" name="especialidadNombre" placeholder="especialidad" value="{{ old('especialidadNombre') }}" required autocomplete="especialidadNombre" autofocus onkeypress="return soloLetras(event)" onpaste="return false" maxlength="30">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('especialidadNombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>


                        

                           
        
  
        <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#especialidadDepartamento").change(event => {
	$.get("especialidad/departamento/"+event.target.value+"", function(res, sta){
		$("#especialidadMunicipio").empty();
		res.forEach(element => {
			$("#especialidadMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
