@extends('admin.template')

@section('tituloTab')
Index    
@endsection

@section('tituloP')
Bienvenido 
@endsection

@section('contenido')

<div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item"><img src="{{asset('img/carrousel/1.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
        <div class="item"><img src="{{asset('img/carrousel/2.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
        <div class="item"><img src="{{asset('img/carrousel/3.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
        <div class="item"><img src="{{asset('img/carrousel/4.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
        <div class="item"><img src="{{asset('img/carrousel/5.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
        <div class="item"><img src="{{asset('img/carrousel/6.jpg')}}" width="1267" height="350" alt="Owl Image"></div>
    </div>

@endsection