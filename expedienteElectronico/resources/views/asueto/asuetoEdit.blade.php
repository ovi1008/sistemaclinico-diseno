@extends('admin.template')

@section('tituloTab')
ASUETO  
@endsection

@section('tituloP')
REGISTRAR ASUETO
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($asueto,['method'=>'PATCH','route'=>['asueto.update',$asueto->id]])!!}
{{Form::token()}}       
            <div class="row">
                <div class=" form-group{{ $errors->has('asuetoNombreDia') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre del Asueto</label>
                        <input id="asuetoNombreDia" type="text" class="form-control" name="asuetoNombreDia" placeholder="Motivo del asueto" value="{{$asueto->asuetoNombreDia}}" required autocomplete="asuetoNombreDia" autofocus onkeypress="return soloLetrasE(event)"  onpaste="return false" maxlength="50">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('asuetoNombreDia')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                <div class="form-group{{ $errors->has('asuetoFecha') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Fecha del Asueto</label>
                        <input data-mask="99/99" id="asuetoFecha" type="text" class="form-control" name="asuetoFecha" placeholder="Fecha del asueto DD/MM" value="{{$asueto->asuetoFecha}}"  autocomplete="asuetoFecha" autofocus onpaste="return false" maxlength="5" pattern="([0-3]{1}[0-9]{1}([\/][0-1]{1}[0-9]{1})?)" title="12/12">

                                <div class="help-block with-errors">
                                        @error('asuetoFecha')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>

    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#asuetoDepartamento").change(event => {
	$.get("asueto/departamento/"+event.target.value+"", function(res, sta){
		$("#asuetoMunicipio").empty();
		res.forEach(element => {
			$("#asuetoMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
