@extends('admin.template')

@section('tituloTab')
Asuetos   
@endsection

@section('tituloP')
Detalle del asueto
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('AsuetoController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Codigo: </td>
                        <td> {{$asueto->id}}</td>
                </tr>
                        
                    <tr>
                        <td >Dia de Asueto: </td>
                        <td> {{$asueto->asuetoNombreDia}}</td>
                    </tr>
                    <tr>
                            <td >fecha del asueto: </td>
                            <td> {{$asueto->asuetoFecha}}</td>
                    </tr>
                                       
                    
                        
                </tbody>
            </table>

@endsection
