@extends('admin.template')

@section('tituloTab')
Usuarios  
@endsection

@section('tituloP')
Crear Usuario
@endsection

@section('contenido')
<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::open(array('url'=>'/user','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}  

    <div class="form-group{{ $errors->has('usuarioAvatar') ? ' has-error' : '' }}">
    <div class="form-group">
        <label for="usuarioAvatar" class="col-md-4 control-label"> Foto </label>
        <input type="file" id="usuarioAvatar" class="form-control" name="usuarioAvatar">
        <div class="help-block with-errors">
                @error('usuarioAvatar')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div> 
        </div>
      </div>

    <div class="form-group">
            <label for="inputName1" class="control-label">Nombres</label>
            <div class="row">
                <div class=" col-md-4{{ $errors->has('usuarioNombre') ? ' has-error' : '' }}">
                        <input id="usuarioNombre" type="text" class="form-control" name="usuarioNombre" placeholder="nombre1" value="{{ old('usuarioNombre') }}" required autocomplete="usuarioNombre" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('usuarioNombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
                <div class=" col-md-4{{ $errors->has('usuarioNombre2') ? ' has-error' : '' }}">
                        <input id="usuarioNombre2" type="text" class="form-control" name="usuarioNombre2" placeholder="nombre2" value="{{ old('usuarioNombre2') }}"  autocomplete="usuarioNombre2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                <div class="help-block with-errors">
                                        @error('usuarioNombre2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                </div>
                <div class=" col-md-4{{ $errors->has('usuarioNombre3') ? ' has-error' : '' }}">
                        <input id="usuarioNombre3" type="text" class="form-control" name="usuarioNombre3" placeholder="nombre3" value="{{ old('usuarioNombre3') }}"  autocomplete="usuarioNombre3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                
                                <div class="help-block with-errors">
                                        @error('usuarioNombre3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
            </div>
        </div>

        <div class="form-group">
                <label for="inputName1" class="control-label">Apellidos</label>
                <div class="row">
                    <div class=" col-md-4{{ $errors->has('usuarioApellido') ? ' has-error' : '' }}">
                            <input id="usuarioApellido" type="text" class="form-control" name="usuarioApellido" placeholder="apellido1" value="{{ old('usuarioApellido') }}" required autocomplete="usuarioApellido" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                            <div class="col-sm-12">
                                    <span class="help-block">
                                        <small>
                                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                        </small>
                                    </span> 
                                </div>
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('usuarioApellido2') ? ' has-error' : '' }}">
                            <input id="usuarioApellido2" type="text" class="form-control" name="usuarioApellido2" placeholder="apellido2" value="{{ old('usuarioApellido2') }}"  autocomplete="usuarioApellido2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido2')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('usuarioApellido3') ? ' has-error' : '' }}">
                            <input id="usuarioApellido3" type="text" class="form-control" name="usuarioApellido3" placeholder="apellido3" value="{{ old('usuarioApellido3') }}"  autocomplete="usuarioApellido3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido3')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('usuarioDui') ? ' has-error' : '' }}">
                    <label for="inputEmail" class="control-label">Dui</label>
                    <input id="usuarioDui" type="text"  data-mask="99999999-9" class="form-control" name="usuarioDui" value="{{ old('usuarioDui') }}" required autocomplete="usuarioDui" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="10" pattern="([0-9]{8}([\-][0-9]{1})?)" title="01873222-2">
                    <span class="font-13 text-muted">99999999-9</span>
                    <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>
            
                                            
                    <div class="help-block with-errors">
                            @error('usuarioDui')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group {{ $errors->has('usuarioNit') ? ' has-error' : '' }}">
                        <label for="inputEmail" class="control-label">Nit</label>
                        <input id="usuarioNit" data-mask="9999-999999-999-9" type="usuarioNit" class="form-control" name="usuarioNit" value="{{ old('usuarioNit') }}" required autocomplete="usuarioNit" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="17" pattern="(([0-9]{4})([\-][0-9]{6})([\-][0-9]{3})([\-][0-9]{1})?)" title="1212-300894-104-8">
                        <span class="font-13 text-muted">9999-999999-999-9</span>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
                
                                                
                        <div class="help-block with-errors">
                                @error('usuarioNit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
 
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="inputEmail" class="control-label">Correo</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">
        <div class="col-sm-12">
                <span class="help-block">
                    <small>
                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                    </small>
                </span> 
            </div>

                                
        <div class="help-block with-errors">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group{{ $errors->has('usuarioTipo') ? ' has-error' : '' }}">
            <label class="control-label">Tipo Usuario</label>
                <select name="usuarioTipo" id="usuarioTipo"class="form-control" onchange="ShowSelected();"required>
                    <option value="">---Seleccion el Tipo de Usuario---</option>
                    <option value="0">Doctor Jefe</option>
                    <option value="1" >Enfermera</option>
                    <option value="2" >Doctor</option>
                    <option value="3" >Secretaria</option>
                </select>
                <div class="help-block with-errors">
                        @error('usuarioTipo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-sm-12">
                        <span class="help-block">
                            <small>
                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                            </small>
                        </span> 
                    </div>

        </div>
        <div id="contenidoJrv">

        <div class="form-group{{ $errors->has('usuarioJrv') ? ' has-error' : '' }}" id="contentJrv" name="contentJrv">
                <label id="labelJrv" for="inputEmail" class="control-label">JRV</label>
                <input id="usuarioJrv" type="text" name="usuarioJrv" value="{{ old('usuarioJrv') }}"  class="form-control" autocomplete="usuarioJrv" onkeypress="return soloNumeros(event)" onpaste="return false" maxlength="6">
                                                       
                <div class="help-block with-errors">
                        @error('usuarioJrv')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div id=espeCon>
            <div class="form-group{{ $errors->has('idEspecialidad') ? ' has-error' : '' }}">
                <label class="control-label">Especialidad Medica</label>
                    <select name="idEspecialidad" id="idEspecialidad"class="form-control">
                        <option value="">---Seleccion la especialidad---</option>
                        @foreach ($especialidad as $espe)
                        <option value="{{$espe->id}}">{{$espe->especialidadNombre}}</option>  
                        @endforeach
                    </select>
                    <div class="help-block with-errors">
                            @error('idEspecialidad')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>
    
            </div>
        </div>
        </div>
          
    <div class="form-group">
        <label for="inputPassword" class="control-label">Contraseña</label>
        <div class="row">
            <div class="form-group col-sm-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" minlength="8">
                    <div class="col-sm-12">
                            <span class="help-block">
                                Minimo 8 caracteres
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                     </div>
            <div class="form-group col-sm-6">
                    <input id="password-confirm" type="password" class="form-control" placeholder="confirmar contraseña" name="password_confirmation" required autocomplete="new-password" minlength="8">
                    <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>
                <div class="help-block with-errors">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script type="text/javascript">
$("#contenidoJrv").hide();
function ShowSelected()
{
/* Para obtener el valor */
var cod = document.getElementById("usuarioTipo").value;
if(cod==0 || cod==2){
    $("#contenidoJrv").show();
    $("#espeCon").show();
    document.getElementById("labelJrv").innerHTML= "JVPM";    
}
else{
    $("#contenidoJrv").hide();
    document.getElementById("usuarioJrv").value = "";
    document.getElementById("idEspecialidad").value = "";
}

if(cod==1){
    $("#contenidoJrv").show();
    $("#espeCon").hide();
    document.getElementById("idEspecialidad").value = "";
    document.getElementById("labelJrv").innerHTML= "JVPE";     
}

}   
    </script>
    
@endsection
