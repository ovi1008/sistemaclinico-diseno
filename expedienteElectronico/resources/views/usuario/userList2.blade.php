@extends('admin.template')

@section('tituloTab')
Usuarios  
@endsection

@section('tituloP')
Lista de Usuarios
@endsection


@section('contenido')

<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<a href="{{URL::action('UserController@index')}}">
				<button  type="submit" class="btn btn-warning btn-rounded" data-toggle="modal" data-target="#add-contact">Activos</button>
				</a>
				<br><br><br>
			<div class="scrollable">
				<div class="table-responsive">
					<table id="tablaUsuario" class="table m-t-30 table-hover contact-list" data-page-size="10">
						<thead>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Rol</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($userT as $use)
							<tr>
									<td>
										<a href="{{url("user/{$use->id}/detalle")}}"><img src="{{asset('img/users/'.$use->usuarioAvatar)}}" alt="{{$use->usuarioNombre}}" class="img-circle" /> {{$use->usuarioNombre}}&nbsp;{{$use->usuarioApellido}}</a>
									</td>
								<td>{{$use->email}}</td>
									<td>
											@if(($use->usuarioTipo)=="0")
											<span class="label label-danger">Doctor Jefe</span> 
											@elseif(($use->usuarioTipo)=="1")
											<span class="label label-info">Enfermera</span>
											@elseif(($use->usuarioTipo)=="2")
											<span class="label label-success">Doctor</span>			
											@elseif(($use->usuarioTipo)=="3")
											<span class="label label-warning">Secretaria</span>
											@endif
										</td>
									<td>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-20" href="{{URL::action('UserController@edit',$use->id)}}"><i class="ti-pencil-alt"></i></a>
											<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$use->id}}" data-toggle="modal"><i class="ti-upload"></i></a>
										</form>	
									</td>
								</tr>
								@include('usuario.userModal2')
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									<a href="{{URL::action('UserController@create')}}">
									<button  type="submit" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#add-contact">Nuevo Usuario</button>
									</a>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>






@endsection

@section('javaPersonalizado')
<script>
		$(document).ready(function() {
			$('#tablaUsuario').DataTable();
			$(document).ready(function() {
				var table = $('#example').DataTable({
					"columnDefs": [{
						"visible": false,
						"targets": 2
					}],

					"displayLength": 25,
					"drawCallback": function(settings) {
						var api = this.api();
						var rows = api.rows({
							page: 'current'
						}).nodes();
						var last = null;
						api.column(2, {
							page: 'current'
						}).data().each(function(group, i) {
							if (last !== group) {
								$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
								last = group;
							}
						});
					}
				});
				// Order by the grouping
				$('#example tbody').on('click', 'tr.group', function() {
					var currentOrder = table.order()[0];
					if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
						table.order([2, 'desc']).draw();
					} else {
						table.order([2, 'asc']).draw();
					}
				});
			});
		});
		$('#example23').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
		$('#tablaUsuario').DataTable( {
			"lengthMenu": [ [5, 10, 20, -1], [5, 10, 120, "All"] ],
  			 "pageLength": 5,
			   "aaSorting": [],
			   "aoColumns": [ null, null, null, { "bSortable": false } ],
			
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
	

		</script>
		
	
@endsection