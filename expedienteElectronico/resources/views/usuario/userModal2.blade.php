<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$use->id}}">
		{!! Form::open(array('url' => 'usuario/estado2/'.$use->id,'method' => 'POST')) !!}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title">Activar Usuario</h4>
				</div>
				<div class="modal-body">
					<b>¿Desea activar al usuario {{$use->usuarioNombre}} {{$use->usuarioApellido}} con dui {{$use->usuarioDui}}?</b> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger">Confirmar</button>
				</div>
			</div>
		</div>
	{{Form::Close()}}
</div>