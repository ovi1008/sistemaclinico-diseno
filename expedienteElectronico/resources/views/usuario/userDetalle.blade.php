@extends('admin.template')

@section('tituloTab')
Usuarios   
@endsection

@section('tituloP')
Detalle del Usuario 
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('UserController@index')}}"><i class="ti-back-left"></i></a>


<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td >Nombre 1: </td>
                        <td> {{$user->usuarioNombre}}</td>
                    </tr>
                    <tr>
                            <td >Nombre 2: </td>
                            <td> {{$user->usuarioNombre2}}</td>
                    </tr>
                    <tr>
                            <td >Nombre 3: </td>
                            <td> {{$user->usuarioNombre3}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 1: </td>
                            <td> {{$user->usuarioApellido}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 2: </td>
                            <td> {{$user->usuarioApellido2}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 3: </td>
                            <td> {{$user->usuarioApellido3}}</td>
                    </tr>
                    <tr>
                            <td >Dui: </td>
                            <td> {{$user->usuarioDui}}</td>
                    </tr>
                    <tr>
                            <td >Nit: </td>
                            <td> {{$user->usuarioNit}}</td>
                    </tr>
                    <tr>
                            <td >Correo: </td>
                            <td> {{$user->email}}</td>
                    </tr>
                     
                                         @if ($control==0)
                                        @else
                                        <tr>
                                        <td >Especialidad de Medicina: </td>
                                        <td>
                                        {{$esp->especialidadNombre}}
                                        </td>
                                        </tr>
                                        @endif
                        
                    

                                    @if(($user->usuarioTipo)=="0")
                                    <tr>
                                        <td >Tipo Usuario: </td>
                                        <td>
                                    <span class="label label-danger">Doctor Jefe</span> </td>

                                </tr>
                                <tr>
                                        <td >JVPM: </td>
                                        <td> {{$user->usuarioJrv}}</td>
                                </tr>
                                    @elseif(($user->usuarioTipo)=="1")
                                    <tr>
                                        <td >Tipo Usuario: </td>
                                        <td>
                                    <span class="label label-info">Enfermera</span>
                                </tr>
                                <tr>
                                        <td >JVPE: </td>
                                        <td> {{$user->usuarioJrv}}</td>
                                </tr>
                                    @elseif(($user->usuarioTipo)=="2")
                                    <tr>
                                        <td >Tipo Usuario: </td>
                                        <td>
                                    <span class="label label-success">Doctor</span>
                                </tr>
                                <tr>
                                        <td >JVPM: </td>
                                        <td> {{$user->usuarioJrv}}</td>
                                </tr>			
                                    @elseif(($user->usuarioTipo)=="3")
                                    <tr>
                                        <td >Tipo Usuario: </td>
                                        <td>
                                    <span class="label label-warning">Secretaria</span>
                                </tr>
                                    @endif                                        
                    <tr>
                                <td >Foto: </td>
                                <td><img src="{{asset('img/users/'.$user->usuarioAvatar)}}" class="img-responsive" /></td>
                        </tr>
                </tbody>
                
            </table>

@endsection
