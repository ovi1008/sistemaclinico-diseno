@extends('admin.template')

@section('tituloTab')
Usuarios  
@endsection

@section('tituloP')
ACtualizar Usuario
@endsection

@section('contenido')
<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>



{!!Form::model($user,['method'=>'PATCH','route'=>['user.update',$user->id],'files'=>'true'])!!}
{!! Form::hidden('id', $user->id) !!}
{{Form::token()}}

    <div class="form-group{{ $errors->has('usuarioAvatar') ? ' has-error' : '' }}">
    <div class="form-group">
        <label for="usuarioAvatar" class="col-md-4 control-label"> Foto </label>
        <input type="file" id="usuarioAvatar" class="form-control" name="usuarioAvatar">
        <br>
        @if(($user->usuarioAvatar)!=" ")
                      <img class="img-thumbnail" src="{{asset('img/users/'.$user->usuarioAvatar)}}" alt="{{$user->usuarioAvatar}}" height="110px" width="110px">
                   @endif
        <div class="help-block with-errors">
                @error('usuarioAvatar')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div> 
        </div>
      </div>

    <div class="form-group">
            <label for="inputName1" class="control-label">Nombres</label>
            <div class="row">
                <div class=" col-md-4{{ $errors->has('usuarioNombre') ? ' has-error' : '' }}">
                        <input id="usuarioNombre" type="text" class="form-control" name="usuarioNombre" placeholder="nombre1" value="{{ old('usuarioNombre',$user->usuarioNombre) }}" required autocomplete="usuarioNombre" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('usuarioNombre')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
                <div class=" col-md-4{{ $errors->has('usuarioNombre2') ? ' has-error' : '' }}">
                        <input id="usuarioNombre2" type="text" class="form-control" name="usuarioNombre2" placeholder="nombre2" value="{{ old('usuarioNombre2',$user->usuarioNombre2) }}"  autocomplete="usuarioNombre2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                <div class="help-block with-errors">
                                        @error('usuarioNombre2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                </div>
                <div class=" col-md-4{{ $errors->has('usuarioNombre3') ? ' has-error' : '' }}">
                        <input id="usuarioNombre3" type="text" class="form-control" name="usuarioNombre3" placeholder="nombre3" value="{{ old('usuarioNombre3',$user->usuarioNombre3) }}"  autocomplete="usuarioNombre3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                
                                <div class="help-block with-errors">
                                        @error('usuarioNombre3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
            </div>
        </div>

        <div class="form-group">
                <label for="inputName1" class="control-label">Apellidos</label>
                <div class="row">
                    <div class=" col-md-4{{ $errors->has('usuarioApellido') ? ' has-error' : '' }}">
                            <input id="usuarioApellido" type="text" class="form-control" name="usuarioApellido" placeholder="apellido1" value="{{ old('usuarioApellido',$user->usuarioApellido) }}" required autocomplete="usuarioApellido" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                            <div class="col-sm-12">
                                    <span class="help-block">
                                        <small>
                                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                        </small>
                                    </span> 
                                </div>
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('usuarioApellido2') ? ' has-error' : '' }}">
                            <input id="usuarioApellido2" type="text" class="form-control" name="usuarioApellido2" placeholder="apellido2" value="{{ old('usuarioApellido2',$user->usuarioApellido2) }}"  autocomplete="usuarioApellido2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido2')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('usuarioApellido3') ? ' has-error' : '' }}">
                            <input id="usuarioApellido3" type="text" class="form-control" name="usuarioApellido3" placeholder="apellido3" value="{{ old('usuarioApellido3',$user->usuarioApellido3) }}"  autocomplete="usuarioApellido3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('usuarioApellido3')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group {{ $errors->has('usuarioDui') ? ' has-error' : '' }}">
                    <label for="inputEmail" class="control-label">Dui</label>
                    <input id="usuarioDui" data-mask="99999999-9" type="text" class="form-control" name="usuarioDui" value="{{ old('usuarioDui',$user->usuarioDui) }}" required autocomplete="usuarioDui" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="10" pattern="([0-9]{8}([\-][0-9]{1})?)" title="01873222-2">
                    <span class="font-13 text-muted">99999999-9</span>
                    <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>
            
                                            
                    <div class="help-block with-errors">
                            @error('usuarioDui')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group {{ $errors->has('usuarioNit') ? ' has-error' : '' }}">
                        <label for="inputEmail" class="control-label">Nit</label>
                        <input id="usuarioNit" data-mask="9999-999999-999-9" type="usuarioNit" class="form-control" name="usuarioNit" value="{{ old('usuarioNit',$user->usuarioNit) }}" required autocomplete="usuarioNit" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="17" pattern="(([0-9]{4})([\-][0-9]{6})([\-][0-9]{3})([\-][0-9]{1})?)" title="1212-300894-104-8">
                        <span class="font-13 text-muted">9999-999999-999-9</span>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
                
                                                
                        <div class="help-block with-errors">
                                @error('usuarioNit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
    
 
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="inputEmail" class="control-label">Correo</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email',$user->email) }}" required autocomplete="email">
        <div class="col-sm-12">
                <span class="help-block">
                    <small>
                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                    </small>
                </span> 
            </div>

                                
        <div class="help-block with-errors">
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <input id="tipo" type="hidden" class="form-control" name="tipo" value="{{ old('tipo',$user->usuarioTipo) }}">
 
    <div class="form-group{{ $errors->has('usuarioTipo') ? ' has-error' : '' }}">
            <label class="control-label">Tipo Usuario</label>
                <select name="usuarioTipo" id="usuarioTipo"class="form-control" onchange="ShowSelected();"required>
                    <option value="">---Seleccion el Tipo de Usuario---</option>
                    @if ($user->usuarioTipo==0)
                    <option value="0" selected>Doctor Jefe</option>                        
                    @else
                    <option value="0">Doctor Jefe</option>                        
                    @endif
                    @if ($user->usuarioTipo==1)
                    <option value="1" selected>Enfermera</option>                        
                    @else
                    <option value="1">Enfermera</option>                        
                    @endif
                    @if ($user->usuarioTipo==2)
                    <option value="2" selected>Doctor</option>                        
                    @else
                    <option value="2">Doctor</option>                        
                    @endif
                    @if ($user->usuarioTipo==3)
                    <option value="3" selected>Secretaria</option>                        
                    @else
                    <option value="3">Secretaria</option>                        
                    @endif
                </select>
                <div class="help-block with-errors">
                        @error('usuarioTipo')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-sm-12">
                        <span class="help-block">
                            <small>
                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                            </small>
                        </span> 
                    </div>

        </div>
            @if ($user->usuarioTipo==3)
                
            @else
            <div id="contenJrv">

                    <div class="form-group{{ $errors->has('usuarioJrv') ? ' has-error' : '' }}" id="contentJrv2" name="contentJrv">
                            <label for="inputEmail" class="control-label">JRV</label>
                            <input id="usuarioJrv" type="text" name="usuarioJrv" value="{{ old('usuarioJrv',$user->usuarioJrv) }}"  class="form-control" autocomplete="usuarioJrv" onkeypress="return soloNumeros(event)" onpaste="return false" maxlength="6">
                                                                   
                            <div class="help-block with-errors">
                                    @error('usuarioJrv')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                <div id=espeCon>
                <div class="form-group{{ $errors->has('idEspecialidad') ? ' has-error' : '' }}">
                    <label class="control-label">Especialidad Medica</label>
                        <select name="idEspecialidad" id="idEspecialidad"class="form-control" >
                            <option value="">---Seleccion la especialidad---</option>
                            @foreach ($especialidad as $espe)                   
                            @if ($user->idEspecialidad == $espe->id)
                            <option value="{{$espe->id}}" selected>{{$espe->especialidadNombre}}</option>   
                            @else
                            <option value="{{$espe->id}}">{{$espe->especialidadNombre}}</option> 
                            @endif                        
                            @endforeach
                        </select>
                        <div class="help-block with-errors">
                                @error('idEspecialidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
        
                </div>
            </div>
            </div>
          
                
            @endif
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script type="text/javascript">
function ShowSelected()
{
/* Para obtener el valor */
var cod = document.getElementById("usuarioTipo").value;
if(cod==0 || cod==2){
    $("#contenidoJrv").show();
    $("#espeCon").show();
    document.getElementById("labelJrv").innerHTML= "JVPM";       
}
else{
    $("#contenidoJrv").hide();
    document.getElementById("usuarioJrv").value = "";
    document.getElementById("idEspecialidad").value = "";
}

if(cod==1){
    $("#contenidoJrv").show();
    $("#espeCon").hide();
    document.getElementById("idEspecialidad").value = "";
    document.getElementById("labelJrv").innerHTML= "JVPE";     
}
}   
    </script>
    
@endsection
