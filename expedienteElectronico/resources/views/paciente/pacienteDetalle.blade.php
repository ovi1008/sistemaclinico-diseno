@extends('admin.template')

@section('tituloTab')
Pacientes   
@endsection

@section('tituloP')
Detalle del paciente 
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('PacienteController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Codigo: </td>
                        <td> {{$paciente->pacienteCodigo}}</td>
                </tr>
                        
                    <tr>
                        <td >Nombre 1: </td>
                        <td> {{$paciente->pacienteNombre1}}</td>
                    </tr>
                    <tr>
                            <td >Nombre 2: </td>
                            <td> {{$paciente->pacienteNombre2}}</td>
                    </tr>
                    <tr>
                            <td >Nombre 3: </td>
                            <td> {{$paciente->pacienteNombre3}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 1: </td>
                            <td> {{$paciente->pacienteApellido1}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 2: </td>
                            <td> {{$paciente->pacienteApellido2}}</td>
                    </tr>
                    <tr>
                            <td >Apellido 3: </td>
                            <td> {{$paciente->pacienteApellido3}}</td>
                    </tr>
                    <tr>
                                <td >Sexo: </td>
                                <td>
                                                @if(($paciente->pacienteSexo)=="0")
                                                <span class="label label-info">Masculino</span> </td>
                                                @elseif(($paciente->pacienteSexo)=="1")
                                                <span class="label label-danger">Femenino</span>

                                                @endif
                                </td>
                        </tr>
                        <tr>
                                        <td >Estado Civil: </td>
                                        <td>@if(($paciente->pacienteEstadoCivil)=="0")
                                                <span class="label label-danger">Soltera/o</span> </td>
                                                @elseif(($paciente->pacienteEstadoCivil)=="1")
                                                <span class="label label-info">Casada/o</span>
                                                @elseif(($paciente->pacienteEstadoCivil)=="2")
                                                <span class="label label-success">Viuda/o</span>			
                                                @elseif(($paciente->pacienteEstadoCivil)=="3")
                                                <span class="label label-warning">Divorciada/o</span>
                                                @endif
                                        </td>
                                </tr>
                        <tr>
                                        <td >Religion: </td>
                                        <td> {{$paciente->pacienteReligion}}</td>
                                </tr>
                <tr>
                        <td >Fecha de nacimiento: </td>
                        <td> {{$paciente->pacienteFechaNacimiento}}</td>
                </tr>
                <tr>
                                <td >Direccion: </td>
                                <td> {{$paciente->pacienteDireccion}}</td>
                        </tr>

                        <tr>
                                        <td >Departamento: </td>
                                        <td> {{$departamento->DepName}}</td>
                                </tr>

                                <tr>
                                                <td >Municipio: </td>
                                                <td> {{$municipio->MunName}}</td>
                                        </tr>
                <tr>
                        <td >Profesion: </td>
                        <td> {{$paciente->pacienteProfesion}}</td>
                </tr>
                    <tr>
                            <td >Dui: </td>
                            <td> {{$paciente->pacienteDui}}</td>
                    </tr>
                    <tr>
                            <td >Nit: </td>
                            <td> {{$paciente->pacienteNit}}</td>
                    </tr>
                    <tr>
                            <td >Contacto de emergencia: </td>
                            <td> {{$paciente->pacientePersonaEmergencia}}</td>
                    </tr>
                    <tr>
                                <td >Contacto de telefono: </td>
                                <td> {{$paciente->pacientePersonaEmergenciaTelefono}}</td>
                        </tr>
                    <tr>
                            <td >Tipo de sangre: </td>
                            <td>{{$paciente->pacienteTipoDeSangre}}</td>
                    </tr>
                </tbody>
            </table>

@endsection
