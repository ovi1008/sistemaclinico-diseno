@extends('admin.template')

@section('tituloTab')
Pacientes  
@endsection

@section('tituloP')
Editar Paciente 
@endsection

@section('contenido')


@include('mensajes.errores')

<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($paciente,['method'=>'PATCH','route'=>['paciente.update',$paciente->id]])!!}
{{Form::token()}}
    <div class="form-group">
            <label for="inputName1" class="control-label">Nombres</label>
            <div class="row">
                <div class=" col-md-4{{ $errors->has('pacienteNombre') ? ' has-error' : '' }}">
                        <input id="pacienteNombre1" type="text" class="form-control" name="pacienteNombre1" placeholder="nombre1" value="{{$paciente->pacienteNombre1}}" required autocomplete="pacienteNombre1" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('pacienteNombre1')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
                <div class=" col-md-4{{ $errors->has('pacienteNombre2') ? ' has-error' : '' }}">
                        <input id="pacienteNombre2" type="text" class="form-control" name="pacienteNombre2" placeholder="nombre2" value="{{$paciente->pacienteNombre2}}"  autocomplete="pacienteNombre2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                <div class="help-block with-errors">
                                        @error('pacienteNombre2')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                </div>
                <div class=" col-md-4{{ $errors->has('pacienteNombre3') ? ' has-error' : '' }}">
                        <input id="pacienteNombre3" type="text" class="form-control" name="pacienteNombre3" placeholder="nombre3" value="{{$paciente->pacienteNombre3}}"  autocomplete="pacienteNombre3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">

                                
                                <div class="help-block with-errors">
                                        @error('pacienteNombre3')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
            </div>
        </div>

        <div class="form-group">
                <label for="inputName1" class="control-label">Apellidos</label>
                <div class="row">
                    <div class=" col-md-4{{ $errors->has('pacienteApellido1') ? ' has-error' : '' }}">
                            <input id="pacienteApellido1" type="text" class="form-control" name="pacienteApellido1" placeholder="apellido1" value="{{$paciente->pacienteApellido1}}" required autocomplete="pacienteApellido1" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
                            <div class="col-sm-12">
                                    <span class="help-block">
                                        <small>
                                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                        </small>
                                    </span> 
                                </div>
    
                                    
                                    <div class="help-block with-errors">
                                            @error('pacienteApellido1')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('pacienteApellido2') ? ' has-error' : '' }}">
                            <input id="pacienteApellido2" type="text" class="form-control" name="pacienteApellido2" placeholder="apellido2" value="{{$paciente->pacienteApellido2}}"  autocomplete="pacienteApellido2" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('pacienteApellido2')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                    <div class=" col-md-4{{ $errors->has('pacienteApellido3') ? ' has-error' : '' }}">
                            <input id="pacienteApellido3" type="text" class="form-control" name="pacienteApellido3" placeholder="apellido3" value="{{$paciente->pacienteNombre3}}"  autocomplete="pacienteApellido3" autofocus onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="30">
    
                                    
                                    <div class="help-block with-errors">
                                            @error('pacienteApellido3')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group{{ $errors->has('pacienteSexo') ? ' has-error' : '' }}">
                    <label class="control-label">Sexo del paciente</label>
                        <select name="pacienteSexo" id="pacienteSexo"class="form-control" onchange="ShowSelected();"required>
                            <option value="">---Seleccione el sexo---</option>
                            @if ($paciente->pacienteSexo==0)
                            <option value="0" selected>Masculino</option>                                
                            @else
                            <option value="0">Masculino</option>
                            @endif
                            @if ($paciente->pacienteSexo==1)
                            <option value="1" selected>Femenino</option>
                            @else
                            <option value="1" >Femenino</option>
                            @endif
                        </select>
                        <div class="help-block with-errors">
                                @error('pacienteSexo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-sm-12">
                                <span class="help-block">
                                    <small>
                                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                    </small>
                                </span> 
                            </div>
        
                </div>
                <div class="form-group{{ $errors->has('pacienteEstadoCivil') ? ' has-error' : '' }}">
                        <label class="control-label">Estado Civil</label>
                            <select name="pacienteEstadoCivil" id="pacienteEstadoCivil"class="form-control" onchange="ShowSelected();"required>
                                <option value="">---Seleccion el estado civil---</option>
                                @if ($paciente->pacienteEstadoCivil==0)
                                <option value="0" selected>Soltera/o</option>   
                                @else
                                <option value="0">Soltera/o</option>
                                @endif
                                @if ($paciente->pacienteEstadoCivil==1)
                                <option value="1" selected>Casada/o</option>   
                                @else
                                <option value="1" >Casada/o</option>
                                @endif
                                @if ($paciente->pacienteEstadoCivil==2)   
                                <option value="2" selected>Viuda/a</option>
                                @else
                                <option value="2" >Viuda/a</option>
                                @endif
                                @if ($paciente->pacienteEstadoCivil==3)   
                                <option value="3" selected>Divorciado/a</option>
                                @else
                                <option value="3" >Divorciado/a</option>
                                @endif                                
                            </select>
                            <div class="help-block with-errors">
                                    @error('pacienteEstadoCivil')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="col-sm-12">
                                    <span class="help-block">
                                        <small>
                                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                        </small>
                                    </span> 
                                </div>
            
                    </div>
                    <div class="form-group {{ $errors->has('pacienteFechaNacimiento') ? ' has-error' : '' }}">
                            <label for="inputEmail" class="control-label">Fecha de nacimiento:</label>
                            <div class="input-group">
                                    <input type="text" data-mask="99/99/9999" class="form-control datepicker" value="{{$paciente->pacienteFechaNacimiento}}"  autocomplete="pacienteFechaNacimiento" required name="pacienteFechaNacimiento" id="pacienteFechaNacimiento"> <span class="input-group-addon"><i class="icon-calender"></i></span> </div>
                                    <span class="font-13 text-muted">dd/mm/aaaa</span>
                                <div class="help-block with-errors">
                                        @error('pacienteFechaNacimiento')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                        </div>

                        <div class="form-group {{ $errors->has('pacienteReligion') ? ' has-error' : '' }}">
                                <label for="inputEmail" class="control-label">Religion del paciente</label>
                                <input id="pacienteReligion" type="text" class="form-control" name="pacienteReligion" value="{{$paciente->pacienteReligion}}"  autocomplete="pacienteReligion" onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="50">                       
                                                        
                                <div class="help-block with-errors">
                                        @error('pacienteReligion')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('pacienteTelefono') ? ' has-error' : '' }}">
                                    <label for="inputEmail" class="control-label">Telefono del paciente</label>
                                    <input id="pacienteTelefono" data-mask="9999-9999" type="text" class="form-control @error('pacienteTelefono') is-invalid @enderror" name="pacienteTelefono" required value="{{$paciente->pacienteTelefono}}"  autocomplete="pacienteTelefono" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="9" pattern="([0-9]{4}([\-][0-9]{4})?)" title="2222-2222">                      
                                    <div class="help-block with-errors">
                                        @error('pacienteTelefono')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                    </div>
                                    <div class="col-sm-12">
                                            <span class="help-block">
                                                <small>
                                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                                </small>
                                            </span> 
                                        </div>
                                </div>

                                <div class="form-group{{ $errors->has('pacienteDepartamento') ? ' has-error' : '' }}">
                                        <label class="control-label">Departamento donde vive</label>
                                            <select name="pacienteDepartamento" id="pacienteDepartamento"class="form-control" required>
                                                <option value="">---Seleccion el departamento---</option>
                                                @foreach ($departamentos as $dep)
                                                @if ($paciente->pacienteDepartamento==$dep->ID)
                                                <option value="{{$dep->ID}}" selected>{{$dep->DepName}}</option>  
                                                @else
                                                <option value="{{$dep->ID}}">{{$dep->DepName}}</option>  
                                                @endif
                                                @endforeach
                                            </select>
                                            <div class="help-block with-errors">
                                                    @error('pacienteDepartamento')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="col-sm-12">
                                                    <span class="help-block">
                                                        <small>
                                                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                                        </small>
                                                    </span> 
                                                </div>
                            
                                    </div>

                                    <div class="form-group{{ $errors->has('pacienteMunicipio') ? ' has-error' : '' }}">
                                            <label class="control-label">Municipio donde vive</label>
                                                <select name="pacienteMunicipio" id="pacienteMunicipio"class="form-control" required>
                                                        <option value="">---Seleccion el departamento---</option>
                                                        @foreach ($municipios as $mun)
                                                        @if ($paciente->pacienteMunicipio==$mun->ID)
                                                        <option value="{{$mun->ID}}" selected>{{$mun->MunName}}</option>  
                                                        @else
                                                        <option value="{{$mun->ID}}">{{$mun->MunName}}</option>  
                                                        @endif
                                                        @endforeach
                                                </select>
                                                <div class="help-block with-errors">
                                                        @error('pacienteMunicipio')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-12">
                                                        <span class="help-block">
                                                            <small>
                                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                                            </small>
                                                        </span> 
                                                    </div>
                                
                                        </div>

                            <div class="form-group {{ $errors->has('pacienteDireccion') ? ' has-error' : '' }}">
                                    <label for="inputEmail" class="control-label">Direccion del paciente</label>
                                    <input id="pacienteDireccion" type="text" class="form-control" name="pacienteDireccion" value="{{$paciente->pacienteDireccion}}" required autocomplete="pacienteDireccion"  maxlength="50">                       
                                                            
                                    <div class="help-block with-errors">
                                            @error('pacienteDireccion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                    <div class="col-sm-12">
                                            <span class="help-block">
                                                <small>
                                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                                </small>
                                            </span> 
                                        </div>
                                </div>

                                <div class="form-group {{ $errors->has('pacienteProfesion') ? ' has-error' : '' }}">
                                        <label for="inputEmail" class="control-label">Profesion del paciente</label>
                                        <input id="pacienteProfesion" type="text" class="form-control" name="pacienteProfesion" value="{{$paciente->pacienteProfesion}}"  autocomplete="pacienteProfesion" onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="50">                       
                                                                
                                        <div class="help-block with-errors">
                                                @error('pacienteProfesion')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>


            <div class="form-group {{ $errors->has('pacienteDui') ? ' has-error' : '' }}">
                    <label for="inputEmail" class="control-label">Dui</label>
                    <input id="pacienteDui" type="text" data-mask="99999999-9" class="form-control" name="pacienteDui" value="{{$paciente->pacienteDui}}"  autocomplete="pacienteDui" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="10" pattern="([0-9]{8}([\-][0-9]{1})?)" title="01873222-2">
                    <span class="font-13 text-muted">99999999-9</span>
                    <div class="help-block with-errors">
                            @error('pacienteDui')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group {{ $errors->has('pacienteNit') ? ' has-error' : '' }}">
                        <label for="inputEmail" class="control-label">Nit</label>
                        <input id="pacienteNit" data-mask="9999-999999-999-9" type="pacienteNit" class="form-control" name="pacienteNit" value="{{$paciente->pacienteNit}}"  autocomplete="pacienteNit" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="17" pattern="(([0-9]{4})([\-][0-9]{6})([\-][0-9]{3})([\-][0-9]{1})?)" title="1212-300894-104-8">
                        <span class="font-13 text-muted">9999-999999-999-9</span>
                        <div class="help-block with-errors">
                                @error('pacienteNit')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
        <div class="form-group {{ $errors->has('pacientePersonaEmergencia') ? ' has-error' : '' }}">
                <label for="inputEmail" class="control-label">Persona en caso de Emergencia del paciente</label>
                <input id="pacientePersonaEmergencia" type="text" class="form-control" name="pacientePersonaEmergencia" value="{{$paciente->pacientePersonaEmergencia}}"  autocomplete="pacientePersonaEmergencia" onkeypress="return soloLetrasE(event)" onpaste="return false" maxlength="60">                       
                                        
                <div class="help-block with-errors">
                        @error('pacientePersonaEmergencia')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
            <div class="form-group{{ $errors->has('pacientePersonaEmergenciaTelefono') ? ' has-error' : '' }}">
                    <label for="inputEmail" class="control-label">Telefono de Persona en caso de emergencia</label>
                    <input id="pacientePersonaEmergenciaTelefono" data-mask="9999-9999" type="text" class="form-control @error('pacientePersonaEmergenciaTelefono') is-invalid @enderror" name="pacientePersonaEmergenciaTelefono" value="{{$paciente->pacientePersonaEmergenciaTelefono}}"  autocomplete="pacientePersonaEmergenciaTelefono" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="9" pattern="([0-9]{4}([\-][0-9]{4})?)" title="2222-2222">                      
                    <div class="help-block with-errors">
                        @error('pacientePersonaEmergenciaTelefono')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                    </div>
                </div>

                <div class="form-group{{ $errors->has('pacienteTipoDeSangre') ? ' has-error' : '' }}">
                        <label class="control-label">Departamento donde vive</label>
                            <select name="pacienteTipoDeSangre" id="pacienteTipoDeSangre"class="form-control" >
                                <option value="">---Seleccion el tipo de sangre--</option>
                                @foreach ($sangres as $san)
                                @if ($paciente->pacienteTipoDeSangre==$san->nombreSangre)
                                <option value="{{$san->nombreSangre}}" selected>{{$san->nombreSangre}}</option>                                     
                                @else
                                <option value="{{$san->nombreSangre}}">{{$san->nombreSangre}}</option>  
                                @endif
                                @endforeach
                            </select>
                            <div class="help-block with-errors">
                                    @error('pacienteTipoDeSangre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>            
                    </div>
        
  
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#pacienteDepartamento").change(event => {
	$.get("paciente/departamento/"+event.target.value+"", function(res, sta){
		$("#pacienteMunicipio").empty();
		res.forEach(element => {
			$("#pacienteMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
