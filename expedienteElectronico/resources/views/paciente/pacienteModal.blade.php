<div class="modal fade modal-slide-in-right" aria-hidden="true" role="dialog" tabindex="-1" id="modal-delete-{{$pacienteA->id}}">
	{{Form::Open(array('action'=>array('PacienteController@destroy',$pacienteA->id),'method'=>'delete'))}}
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h4 class="modal-title">Eliminar APaciente</h4>
				</div>
				<div class="modal-body">
					<b>¿Desea eliminar al paciente {{$pacienteA->pacienteNombre1}} {{$pacienteA->pacienteApellido1}} con codigo {{$pacienteA->pacienteCodigo}}?</b> 
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-danger">Confirmar</button>
				</div>
			</div>
		</div>
	{{Form::Close()}}
</div>