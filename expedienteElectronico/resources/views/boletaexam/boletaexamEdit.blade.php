@extends('admin.template')

@section('tituloTab')
BOLETA  
@endsection

@section('tituloP')
Actualizar BOLETA DE EXAMEN
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($boletaexam,['method'=>'PATCH','route'=>['boletaexam.update',$boletaexam->id]])!!}
{{Form::token()}}  
                <div class=" form-group{{ $errors->has('boletaexamSolicitados') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre de los Medicamentos</label>
                <textarea  id="boletaexamSolicitados" rows="6"  class="form-control" name="boletaexamSolicitados" placeholder="Motivo del boletaexam" value="{{$boletaexam->boletaexamSolicitados}}" required autocomplete="boletaexamSolicitados" maxlength="250">{{$boletaexam->boletaexamSolicitados}}</textarea>
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('boletaexamSolicitados')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#boletaexamDepartamento").change(event => {
	$.get("boletaexam/departamento/"+event.target.value+"", function(res, sta){
		$("#boletaexamMunicipio").empty();
		res.forEach(element => {
			$("#boletaexamMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
