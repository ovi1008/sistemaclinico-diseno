@extends('admin.template')

@section('tituloTab')
BOLETA  
@endsection

@section('tituloP')
REGISTRAR BOLETA
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::open(array('url'=>'/boletaexam','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}             
                <div class=" form-group{{ $errors->has('boletaexamSolicitados') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Nombre de los Examenes solicitados</label>
                        <textarea  id="boletaexamSolicitados" rows="6"  class="form-control" name="boletaexamSolicitados" value="" required autocomplete="boletaexamSolicitados" maxlength="250"> </textarea>
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('boletaexamSolicitados')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                    
                      
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#boletaDepartamento").change(event => {
	$.get("boleta/departamento/"+event.target.value+"", function(res, sta){
		$("#boletaMunicipio").empty();
		res.forEach(element => {
			$("#boletaMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
