@extends('admin.template')

@section('tituloTab')
BOLETA   
@endsection

@section('tituloP')
Detalle del BOLETA
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('boletaexamController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >ID: </td>
                        <td> {{$boletaexam->id}}</td>
                </tr>
                        
                    <tr>
                        <td >Examenes: </td>
                        <td> {{$boletaexam->boletaexamSolicitados}}</td>
                    </tr>
                    
                        
                </tbody>
            </table>

@endsection
