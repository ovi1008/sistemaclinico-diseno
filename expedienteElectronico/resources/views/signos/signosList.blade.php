@extends('admin.template')

@section('tituloTab')
Expediente Clinico
@endsection

@section('tituloP')
Expediente Clinico
@endsection


@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('PacienteController@index')}}"><i class="ti-back-left"></i></a>
<br><br><br>
<h3 class="box-title">Ficha Clinica de:</h3>
<h4 align="center">{{$pacienteA->pacienteNombre1}} {{$pacienteA->pacienteNombre2}} {{$pacienteA->pacienteNombre3}} {{$pacienteA->pacienteApellido1}} {{$pacienteA->pacienteApellido2}} {{$pacienteA->pacienteApellido3}}</h4>

	  
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<br><br>
			<div class="scrollable">
				<div class="table-responsive">
					<table id="tablaPaciente" class="table m-t-30 table-hover contact-list" data-page-size="10">
						<thead>
								<th>Id</th>
								<th>Fecha de la consulta</th>
								<th>Doctor asignado</th>
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($signos as $sig)
							<tr>
								<td>
								<a href="{{URL::action('PreparacionController@show',$sig->id)}}">{{$sig->id}}</a>
								</td>
								<td>{{date ('d-m-Y h:m:s', strtotime ($sig->created_at))}}</td>									
								<td>
									@foreach ($doctores as $doctor)
									@if ($doctor->id == $sig->preparacionAsignacion)
								{{$doctor->usuarioNombre}}&nbsp;{{$doctor->usuarioApellido}}&nbsp;<span class="label label-info">{{$doctor->especialidadNombre}}</span>
									@endif
									@endforeach
								</td>
									<td>
										@if ($sig->preparacionEstado==1)
										@if ((Auth::user()->usuarioTipo==0)||(Auth::user()->usuarioTipo==1)||(Auth::user()->usuarioTipo==2))
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{ route('preparacion.Editar',['id' => $sig->id])}}"><i class="ti-pencil-alt"></i></a>
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$sig->id}}" data-toggle="modal"><i class="icon-trash"></i></a>
										@else
										@endif
										@if ((Auth::user()->usuarioTipo==0)||(Auth::user()->usuarioTipo==2))
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{ route('consulta.Crear',['id' => $sig->id])}}"><i class="ti-clipboard"></i></a>						
										@else
										@endif
										@else
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('ConsultaController@show',$sig->id)}}"><i class="ti-notepad"></i></a>
										@endif
									</td>
								</tr>
								@include('signos.signosModal')								
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="4">
									@if (Auth::user()->usuarioTipo==3)	
									@else
								<a href="{{ route('preparacion.Crear',['id' => $id])}}">
									<button  type="submit" class="btn btn-info btn-rounded" >Crear Signos Vitales</button></a>
									@endif
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javaPersonalizado')
<script>
		$(document).ready(function() {
			$('#tablaPaciente').DataTable();
			$(document).ready(function() {
				var table = $('#example').DataTable({
					"columnDefs": [{
						"visible": false,
						"targets": 2
					}],
					"order": [
						[2, 'asc']
					],
					"displayLength": 25,
					"drawCallback": function(settings) {
						var api = this.api();
						var rows = api.rows({
							page: 'current'
						}).nodes();
						var last = null;
						api.column(2, {
							page: 'current'
						}).data().each(function(group, i) {
							if (last !== group) {
								$(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
								last = group;
							}
						});
					}
				});
				// Order by the grouping
				$('#example tbody').on('click', 'tr.group', function() {
					var currentOrder = table.order()[0];
					if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
						table.order([2, 'desc']).draw();
					} else {
						table.order([2, 'asc']).draw();
					}
				});
			});
		});
		$('#example23').DataTable({
			dom: 'Bfrtip',
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			]
		});
		$('#tablaPaciente').DataTable( {
			"lengthMenu": [ [5, 10, 20, -1], [5, 10, 20, "All"] ],
  			 "pageLength": 5,
			   "aaSorting": [],
			   "aoColumns": [ null, null, null, { "bSortable": false } ],
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
	

		</script>	
@endsection