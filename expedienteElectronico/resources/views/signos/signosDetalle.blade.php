@extends('admin.template')

@section('tituloTab')
Signos vitales  
@endsection

@section('tituloP')
Detalle de signos vitales 
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{ route('preparacion.Listar',['id' => $signo->idPaciente])}}"><i class="ti-back-left"></i></a>


<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                    <tr>
                        <td >Paciente: </td>
                        <td>{{$paciente->pacienteNombre1}} {{$paciente->pacienteNombre2}} {{$paciente->pacienteNombre3}} {{$paciente->pacienteApellido1}} {{$paciente->pacienteApellido2}} {{$paciente->pacienteApellido3}}</td>
                    </tr>
                    <tr>
                            <td >Pulso: </td>
                            <td> {{$signo->preparacionPulso}} mmHg</td>
                    </tr>
                    <tr>
                            <td >Temperatura: </td>
                            <td> {{$signo->preparacionTemperatura}} °C</td>
                    </tr>
                    <tr>
                            <td >Peso: </td>
                            <td> {{$signo->preparacionPeso}} Lb</td>
                    </tr>
                    <tr>
                            <td >Altura: </td>
                            <td> {{$signo->preparacionAltura}} cm</td>
                    </tr>
                    <tr>
                            <td >Doctor Asignado: </td>
                            <td>{{$doctor->usuarioNombre}}&nbsp; {{$doctor->usuarioApellido}}&nbsp;<span class="label label-info">{{$espe->especialidadNombre}}</span></td>
                    </tr>
                    <tr>
                            <td >Estado: </td>
                            <td>
                                        @if(($signo->preparacionEstado)=="0")
                                        <span class="label label-danger">Terminada</span> </td>
                                        @elseif(($signo->preparacionEstado)=="1")
                                        <span class="label label-success">Activa</span>

                                        @endif
                        </td>
                    </tr>
                   
                   
                </tbody>
                
            </table>

@endsection
