@extends('admin.template')

@section('tituloTab')
Signos Vitales
@endsection

@section('tituloP')
Editar Signos Vitales
@endsection

@section('contenido')
<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($signo,['method'=>'PATCH','route'=>['preparacion.update',$signo->id]])!!}
{{Form::token()}}

    <div class="form-group">
            <div class="row">
                <div class=" col-md-4{{ $errors->has('preparacionPulso') ? ' has-error' : '' }}">
                    <label for="inputName1" class="control-label">Pulso</label>
                        <input id="preparacionPulso" type="text" class="form-control" name="preparacionPulso" placeholder="mmHg" value="{{$signo->preparacionPulso}}"  autocomplete="preparacionPulso" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10">
                        <div class="col-sm-12">
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('preparacionPulso')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
                <div class=" col-md-4{{ $errors->has('preparacionTemperatura') ? ' has-error' : '' }}">
                       <label for="inputName1" class="control-label">Temperatura</label>
                        <input id="preparacionTemperatura" type="text" class="form-control" name="preparacionTemperatura" placeholder="℃" value="{{$signo->preparacionTemperatura}}"  autocomplete="preparacionTemperatura" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10">

                                <div class="help-block with-errors">
                                        @error('preparacionTemperatura')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    </div>
                </div>
                <div class=" col-md-4{{ $errors->has('preparacionPeso') ? ' has-error' : '' }}">
                    <label for="inputName1" class="control-label">Peso</label>
                        <input id="preparacionPeso" type="text" class="form-control" name="preparacionPeso" placeholder="Lb" value="{{$signo->preparacionPeso}}"  autocomplete="preparacionPeso" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10">
                                <div class="help-block with-errors">
                                        @error('preparacionPeso')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                </div>
            </div>
        </div>

        <div class="form-group">
                
                <div class="row">
                    
                    <div class=" col-md-4{{ $errors->has('preparacionAltura') ? ' has-error' : '' }}">
                            <label for="inputName1" class="control-label">Altura</label>
                            <input id="preparacionAltura" type="text" class="form-control" name="preparacionAltura" placeholder="cm" value="{{$signo->preparacionAltura}}"  autocomplete="preparacionAltura" autofocus onkeypress="return soloNumerosP(event)" onpaste="return false" maxlength="10">                                    
                                    <div class="help-block with-errors">
                                            @error('preparacionAltura')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        </div>
                    </div>
                </div>
        </div>


    <div class="form-group{{ $errors->has('preparacionAsignacion') ? ' has-error' : '' }}">
            <label class="control-label">Asignacion</label>
                <select name="preparacionAsignacion" id="preparacionAsignacion"class="form-control" required>
                    <option value="">---Seleccion la asignacion---</option>
                    @foreach ($doctores as $doc)
                    @if ($signo->preparacionAsignacion==$doc->id)
                    <option selected value="{{$doc->id}}">{{$doc->usuarioNombre}}&nbsp;{{$doc->usuarioApellido}}---{{$doc->especialidadNombre}}</span></option>  
                    @else
                    <option value="{{$doc->id}}">{{$doc->usuarioNombre}}&nbsp;{{$doc->usuarioApellido}}---{{$doc->especialidadNombre}}</span></option>  
                    @endif
                    @endforeach
                </select>
                <div class="help-block with-errors">
                        @error('preparacionAsignacion')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="col-sm-12">
                        <span class="help-block">
                            <small>
                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                            </small>
                        </span> 
                    </div>

        </div>
    


    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Guardar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script type="text/javascript">
$("#contentJrv").hide();
function ShowSelected()
{
/* Para obtener el valor */
var cod = document.getElementById("usuarioTipo").value;
if(cod==0 || cod==1 || cod==2){
    $("#contentJrv").show();    
}else{
    $("#contentJrv").hide();
    document.getElementById("usuarioJrv").value = "";
}
}   
    </script>
    
@endsection
