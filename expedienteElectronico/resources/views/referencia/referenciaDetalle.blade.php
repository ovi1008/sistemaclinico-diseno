@extends('admin.template')

@section('tituloTab')
Referencias   
@endsection

@section('tituloP')
Detalle del Referencia
@endsection

@section('contenido')
<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('ReferenciaController@index')}}"><i class="ti-back-left"></i></a>

<h3 class="box-title m-t-40">Informacion General</h3>
        <div class="table-responsive">
            <table class="table">
                <tbody>
                <tr>
                        <td >Motivo de la referencia: </td>
                        <td> {{$referencia->referenciaMotivos}}</td>
                </tr>
                        
                    <tr>
                        <td >Observaciones: </td>
                        <td> {{$referencia->referenciaObservaciones}}</td>
                    </tr>
                    
                        
                </tbody>
            </table>

@endsection
