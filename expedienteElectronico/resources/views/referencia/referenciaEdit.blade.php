@extends('admin.template')

@section('tituloTab')
REFERENCIA  
@endsection

@section('tituloP')
Actualizar REFERENCIA
@endsection

@section('contenido')



<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>

{!!Form::model($referencia,['method'=>'PATCH','route'=>['referencia.update',$referencia->id]])!!}
{{Form::token()}}  
                <div class=" form-group{{ $errors->has('referenciaMotivos') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Motivos de la referencia</label>
                <textarea  id="referenciaMotivos" rows="6"  class="form-control" name="referenciaMotivos" placeholder="Motivo del referencia" value="{{$referencia->referenciaMotivos}}" required autocomplete="referenciaMotivos" maxlength="250">{{$referencia->referenciaMotivos}}</textarea>
                        <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>

                                
                                <div class="help-block with-errors">
                                        @error('referenciaMotivos')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                    </div>
                <div class=" form-group{{ $errors->has('referenciaObservaciones') ? ' has-error' : '' }}">
                        <label for="inputName1" class="control-label">Observaciones</label>
                <textarea  id="referenciaObservaciones" rows="6"  class="form-control" name="referenciaObservaciones" placeholder="Motivo del referencia" value="{{$referencia->referenciaObservaciones}}" required autocomplete="referenciaMotivos" maxlength="250">{{$referencia->referenciaObservaciones}}</textarea>                        
                        
                                <div class="help-block with-errors">
                                        @error('referenciaObservaciones')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-sm-12">
                                        <span class="help-block">
                                            <small>
                                                    <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                            </small>
                                        </span> 
                                    </div>
                </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Actualizar</button>
    </div>

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')
<script>
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
</script>
<script type="text/javascript">
$("#referenciaDepartamento").change(event => {
	$.get("referencia/departamento/"+event.target.value+"", function(res, sta){
		$("#referenciaMunicipio").empty();
		res.forEach(element => {
			$("#referenciaMunicipio").append(`<option value=${element.ID}> ${element.MunName} </option>`);
		});
	});
});
</script>

<script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
    </script>

@endsection
