@extends('admin.template')

@section('tituloTab')
referencia  
@endsection

@section('tituloP')
Lista de referencia
@endsection


@section('contenido')
	  
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			@if (session('status'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('status') }}
                        </div>
			@endif
			<div class="scrollable">
				<div class="table-responsive">
					<table id="prueba2" class="display">
						<thead>
							<tr>
								<th>Motivos de referencia </th>
						
								<th>Observaciones</th>
								
								<th>Accion</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($referencia as $referenciaA)
							<tr>
								<td>
								<a href="{{URL::action('ReferenciaController@show',$referenciaA->id)}}">{{$referenciaA->referenciaMotivos }}</a>
								</td>
			
								<td>{{$referenciaA->referenciaObservaciones}}</td>									
								
									<td>
									    <a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="{{URL::action('ReferenciaController@edit',$referenciaA->id)}}"><i class="ti-pencil-alt"></i></a>
										<a class="btn btn-info btn-outline btn-circle btn-lg m-r-5" href="" data-target="#modal-delete-{{$referenciaA->id}}" data-toggle="modal"><i class="icon-trash"></i></a>
									</td>

									
								</tr>
							
							@include('referencia.referenciaModal')									
							@endforeach
							
						</tbody>
						<tfoot>
							<tr>
								<td colspan="3">
									
									<a href="{{URL::action('ReferenciaController@create')}}">
									<button  type="submit" class="btn btn-info btn-rounded" >Añadir referencia</button>
									</a>	
									
								</td>
							</tr>
						</tfoot>
					</table>
				
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('javaPersonalizado')
@section('javaPersonalizado')
		<script>
		$(document).ready( function () {
			$('#prueba2').DataTable();
			} );
		$('#prueba2').DataTable( {
			"lengthMenu": [ [8, 16, 24, -1], [8, 16, 24, "All"] ],
  			 "pageLength": 8,
			   "aaSorting": [],
			   "aoColumns": [ null, null, { "bSortable": false } ],
			
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        }
	} );
			</script>	
@endsection
@endsection