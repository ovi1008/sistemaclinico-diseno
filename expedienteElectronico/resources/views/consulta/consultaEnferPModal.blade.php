<div class="modal fade bs-example-modal-lg" aria-hidden="true" role="dialog" tabindex="-1" id="modal-create-consulta-enfermedadPrincipal">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h1 class="modal-title" style="color:darkslateblue" align="center">Busqueda en catalogo CIE10</h1>
				</div>
				<div class="modal-header">
						<b>¿Escoja una enfermedad?</b> 
				</div>
				<div class="modal-body">

						<div class="form-group{{ $errors->has('consultaEnfermedadP') ? ' has-error' : '' }}">
								<label class="control-label">Enfermedad Principal</label>
									<select name="consultaEnfermedadP" id="consultaEnfermedadP"class="form-control" required >
										<option value="">---Seleccion una enfermedad---</option>
						
									</select>
									<div class="help-block with-errors">
											@error('consultaEnfermedadP')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="col-sm-12">
											<span class="help-block">
												<small>
														<p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
												</small>
											</span> 
										</div>
										<span class="font-13 text-muted">Si la lista le aparece vacias, no se encontraron resultados con el nombre dado.</span>
					
							</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button data-dismiss="modal" 
					aria-label="Close" class="btn btn-success" onclick="textoEnfermedadP()">Confirmar</button>
				</div>
			</div>
		</div>
</div>