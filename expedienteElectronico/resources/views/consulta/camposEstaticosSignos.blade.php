<div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <h3 class="box-title">Signos Vitales</h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Pulso:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$signo->preparacionPulso}} mmHg </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Temperatura</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$signo->preparacionTemperatura}} °C  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Peso</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$signo->preparacionPeso}} Lb </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Altura: </label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$signo->preparacionAltura}} cm </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <!--/row-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>