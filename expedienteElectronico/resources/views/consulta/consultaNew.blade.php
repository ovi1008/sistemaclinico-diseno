@extends('admin.template')

@section('tituloTab')
Consultas 
@endsection

@section('tituloP')
Crear Consulta
@endsection

@section('contenido')
@include('mensajes.errores')
<p class="text-muted m-b-30 font-13">Los campos obligatorios se le muestran marcados.</p>
@include('consulta.camposEstaticosPaciente')
@include('consulta.camposEstaticosSignos')


{!!Form::open(array('url'=>'/consulta','method'=>'POST','autocomplete'=>'off','files'=>true, 'id' => 'my-dropzone'))!!}
{{Form::token()}}  
<input type="hidden" value="{{$id}}" name="idPacientePreparacion" id="idPacientePreparacion">
 
    <div class="form-group{{ $errors->has('consultaConsultaPor') ? ' has-error' : '' }} ">
        <label for="inputEmail" class="control-label">Consulta por:</label>
        <textarea  id="consultaConsultaPor" rows="4"  class="form-control" name="consultaConsultaPor" value="{{ old('consultaConsultaPor') }}" required autocomplete="consultaConsultaPor">{{ old('consultaConsultaPor') }}</textarea>
        <div class="col-sm-12">
                <span class="help-block">
                    <small>
                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                    </small>
                </span> 
            </div>

                                
        <div class="help-block with-errors">
                @error('consultaConsultaPor')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group{{ $errors->has('consultaPresentaEnfermedad') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Presenta Enfermedad:</label>
            <textarea  id="consultaPresentaEnfermedad" rows="4"  class="form-control" name="consultaPresentaEnfermedad" value="{{ old('consultaPresentaEnfermedad') }}" required autocomplete="consultaPresentaEnfermedad">{{ old('consultaPresentaEnfermedad') }}</textarea>
            <div class="col-sm-12">
                    <span class="help-block">
                        <small>
                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                        </small>
                    </span> 
                </div>
                   
            <div class="help-block with-errors">
                    @error('consultaPresentaEnfermedad')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
    <div class="form-group{{ $errors->has('consultaAntecedentesPersonales') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Antecedentes personales:</label>
            <textarea  id="consultaAntecedentesPersonales" rows="4"  class="form-control" name="consultaAntecedentesPersonales" value="{{ old('consultaAntecedentesPersonales') }}"  autocomplete="consultaAntecedentesPersonales">{{ old('consultaAntecedentesPersonales') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaAntecedentesPersonales')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
    <div class="form-group{{ $errors->has('consultaAntecedentesFamiliares') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Antecedentes Familiares:</label>
            <textarea  id="consultaAntecedentesFamiliares" rows="4"  class="form-control" name="consultaAntecedentesFamiliares" value="{{ old('consultaAntecedentesFamiliares') }}"  autocomplete="consultaAntecedentesFamiliares">{{ old('consultaAntecedentesFamiliares') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaAntecedentesFamiliares')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
    <div class="form-group{{ $errors->has('consultaExploracionClinica') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Exploracion clinica:</label>
            <textarea  id="consultaExploracionClinica" rows="4"  class="form-control" name="consultaExploracionClinica" value="{{ old('consultaExploracionClinica') }}" required autocomplete="consultaExploracionClinica">{{ old('consultaExploracionClinica') }}</textarea>
            <div class="col-sm-12">
                    <span class="help-block">
                        <small>
                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                        </small>
                    </span> 
            </div>                              
            <div class="help-block with-errors">
                    @error('consultaExploracionClinica')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

    <div class="form-group{{ $errors->has('consultaDiagnosticoPrincipal') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Diagnostico principal:</label>
            <textarea  id="consultaDiagnosticoPrincipal" rows="4"  class="form-control" name="consultaDiagnosticoPrincipal" value="{{ old('consultaDiagnosticoPrincipal') }}" required autocomplete="consultaDiagnosticoPrincipal">{{ old('consultaDiagnosticoPrincipal') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaDiagnosticoPrincipal')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-sm-12">
                    <span class="help-block">
                        <small>
                                <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                        </small>
                    </span> 
                </div>
    </div>
    <div class="form-group">
            <div class="input-group"> <span class="input-group-btn">
                    <a data-target="#modal-create-consulta-enfermedadPrincipal"  data-toggle="modal" type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></a></span>
                    <input type="text"  id="buscadorPrincipal" name="buscadorPrincipal" class="form-control" placeholder="Buscar enfermedad principal"> </div>
                    <div class="col-sm-12">
                            <span class="help-block">
                                <small>
                                        <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                                </small>
                            </span> 
                        </div>
    </div>
    <div class="form-group">
            <label id="inputEnferP" name="inputEnferP" class="control-label">No ha seleccionado ninguna efermedad principal.</label>
    </div>


    <div class="form-group{{ $errors->has('consultaOtrosDiagnosticos') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Otros diagnosticos:</label>
            <textarea  id="consultaOtrosDiagnosticos" rows="4"  class="form-control" name="consultaOtrosDiagnosticos" value="{{ old('consultaOtrosDiagnosticos') }}"  autocomplete="consultaOtrosDiagnosticos">{{ old('consultaOtrosDiagnosticos') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaOtrosDiagnosticos')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
    </div>

    <div class="form-group{{ $errors->has('consultaEnfermedadS1') ? ' has-error' : '' }}">
            <div class="input-group"> <span class="input-group-btn">
                    <a data-target="#modal-create-consulta-enfermedadSecundaria1"  data-toggle="modal" type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></a></span>
                    <input type="text"  id="buscadorSecundario1" name="buscadorSecundario1" class="form-control" placeholder="Buscar enfermedad secundaria 1"> </div>
                    <div class="help-block with-errors">
                            @error('consultaEnfermedadS1')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
    </div>
    <div class="form-group">
            <label id="inputEnferS1" name="inputEnferS1" class="control-label">No ha seleccionado ninguna efermedad secundaria.</label>
    </div>

    <div class="form-group{{ $errors->has('consultaEnfermedadS2') ? ' has-error' : '' }}">
            <div class="input-group"> <span class="input-group-btn">
                    <a data-target="#modal-create-consulta-enfermedadSecundaria2"  data-toggle="modal" type="button" class="btn waves-effect waves-light btn-info"><i class="fa fa-search"></i></a></span>
                    <input type="text"  id="buscadorSecundario2" name="buscadorSecundario2" class="form-control" placeholder="Buscar enfermedad enfermedad secundaria 2"> </div>
                    <div class="help-block with-errors">
                            @error('consultaEnfermedadS2')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
    </div>
    <div class="form-group">
            <label id="inputEnferS2" name="inputEnferS2" class="control-label">No ha seleccionado ninguna efermedadsecundaria.</label>
    </div>

    <div class="form-group{{ $errors->has('consultaTratamiento') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Tratamiento: </label>
            <textarea  id="consultaTratamiento" rows="4"  class="form-control" name="consultaTratamiento" value="{{ old('consultaTratamiento') }}" required autocomplete="consultaTratamiento">{{ old('consultaTratamiento') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaTratamiento')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="col-sm-12">
                <span class="help-block">
                    <small>
                            <p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
                    </small>
                </span> 
            </div>
    </div>

    <div class="form-group{{ $errors->has('consultaObservaciones') ? ' has-error' : '' }} ">
            <label for="inputEmail" class="control-label">Observaciones:</label>
            <textarea  id="consultaObservaciones" rows="4"  class="form-control" name="consultaObservaciones" value="{{ old('consultaObservaciones') }}"  autocomplete="consultaObservaciones">{{ old('consultaObservaciones') }}</textarea>
                                
            <div class="help-block with-errors">
                    @error('consultaObservaciones')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
    </div>

    <a class="btn btn-block btn-primary" href="" onclick="impresionConsulta()" data-target="#modal-create-consulta" data-toggle="modal">Guardar</a>
    

@include('consulta.consultaEnferPModal')
@include('consulta.consultaEnferS1Modal')
@include('consulta.consultaEnferS2Modal')
@include('consulta.consultaModal')

{!!Form::close()!!}
@endsection

@section('javaPersonalizado')

<script type="text/javascript">
    $("#buscadorPrincipal").change(event => {
        $.get("consulta/enfermedadP/"+event.target.value+"%"+"", function(res, sta){
            $("#consultaEnfermedadP").empty();
            res.forEach(element => {
                $("#consultaEnfermedadP").append(`<option value=${element.cie10Descripcion}> ${element.cie10Descripcion} </option>`);
            });
        });
    });
</script>

<script type="text/javascript">
    $("#buscadorSecundario1").change(event => {
        $.get("consulta/enfermedadP/"+event.target.value+"%"+"", function(res, sta){
            $("#consultaEnfermedadS1").empty();
            res.forEach(element => {
                $("#consultaEnfermedadS1").append(`<option value=${element.cie10Descripcion}> ${element.cie10Descripcion} </option>`);
            });
        });
    });
</script>

<script type="text/javascript">
    $("#buscadorSecundario2").change(event => {
        $.get("consulta/enfermedadP/"+event.target.value+"%"+"", function(res, sta){
            $("#consultaEnfermedadS2").empty();
            res.forEach(element => {
                $("#consultaEnfermedadS2").append(`<option value=${element.cie10Descripcion}> ${element.cie10Descripcion} </option>`);
            });
        });
    });
</script>

<script>
    function textoEnfermedadP(){
        var textoP=document.getElementById("consultaEnfermedadP").value;
        document.getElementById("inputEnferP").innerHTML ="Se ha seleccionado como enfermedad principal: "+textoP;

    }
    function textoEnfermedadS1(){
        var textoS1=document.getElementById("consultaEnfermedadS1").value;
        document.getElementById("inputEnferS1").innerHTML ="Se ha seleccionado como enfermedad secundaria 1: "+textoS1;

    }
    function textoEnfermedadS2(){
        var textoS2=document.getElementById("consultaEnfermedadS2").value;
        document.getElementById("inputEnferS2").innerHTML ="Se ha seleccionado como enfermedad secundaria 2: "+textoS2;

    }
</script>
    

<script>
function impresionConsulta() {
    var parte01CD=document.getElementById("consultaConsultaPor").value;
    document.getElementById("parte01C").innerHTML = parte01CD;

    var parte02CD=document.getElementById("consultaPresentaEnfermedad").value;
    document.getElementById("parte02C").innerHTML = parte02CD;

    var parte03CD=document.getElementById("consultaAntecedentesPersonales").value;
    document.getElementById("parte03C").innerHTML = parte03CD;

    var parte04CD=document.getElementById("consultaAntecedentesFamiliares").value;
    document.getElementById("parte04C").innerHTML = parte04CD;

    var parte05CD=document.getElementById("consultaExploracionClinica").value;
    document.getElementById("parte05C").innerHTML = parte05CD;

    var parte06CD=document.getElementById("consultaDiagnosticoPrincipal").value;
    document.getElementById("parte06C").innerHTML = parte06CD;

    var parte06ACD=document.getElementById("consultaEnfermedadP").value;
    document.getElementById("parte06AC").innerHTML = parte06ACD;

    var parte07CD=document.getElementById("consultaOtrosDiagnosticos").value;
    document.getElementById("parte07C").innerHTML = parte07CD;

    var parte07ACD=document.getElementById("consultaEnfermedadS1").value;
    document.getElementById("parte07AC").innerHTML = parte07ACD;

    var parte07BCD=document.getElementById("consultaEnfermedadS2").value;
    document.getElementById("parte07BC").innerHTML = parte07BCD;

    var parte08CD=document.getElementById("consultaTratamiento").value;
    document.getElementById("parte08C").innerHTML = parte08CD;

    var parte09CD=document.getElementById("consultaObservaciones").value;
    document.getElementById("parte09C").innerHTML = parte09CD;
}
</script>


    
@endsection
