<div class="modal fade bs-example-modal-lg" aria-hidden="true" role="dialog" tabindex="-1" id="modal-create-consulta-enfermedadSecundaria1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h1 class="modal-title" style="color:darkslateblue" align="center">Busqueda en catalogo CIE10</h1>
				</div>
				<div class="modal-header">
						<b>¿Escoja una enfermedad?</b> 
				</div>
				<div class="modal-body">

						<div class="form-group{{ $errors->has('consultaEnfermedadS1') ? ' has-error' : '' }}">
								<label class="control-label">Enfermedad Secundaria 1</label>
									<select name="consultaEnfermedadS1" id="consultaEnfermedadS1"class="form-control">
										<option value="">---Seleccion una enfermedad---</option>
						
									</select>
									<div class="help-block with-errors">
											@error('consultaEnfermedadS1')
											<span class="invalid-feedback" role="alert">
												<strong>{{ $message }}</strong>
											</span>
										@enderror
									</div>
									<div class="col-sm-12">
											<span class="help-block">
												<small>
														<p class="text-muted m-b-30 font-13"><code>obligatorio**</code></p>
												</small>
											</span> 
										</div>
										<span class="font-13 text-muted">Si la lista le aparece vacias, no se encontraron resultados con el nombre dado.</span>
					
							</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button data-dismiss="modal" 
					aria-label="Close" class="btn btn-success" onclick="textoEnfermedadS1()">Confirmar</button>
				</div>
			</div>
		</div>
</div>