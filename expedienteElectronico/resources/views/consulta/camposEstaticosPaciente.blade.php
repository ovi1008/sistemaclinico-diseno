<div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-wrapper collapse in" aria-expanded="true">
                    <div class="panel-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-body">
                                <h3 class="box-title">Informacion del paciente</h3>
                                <hr class="m-t-0 m-b-40">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Nombre:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">{{$paciente->pacienteNombre1}} {{$paciente->pacienteNombre2}} {{$paciente->pacienteNombre3}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Apellido</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">{{$paciente->pacienteApellido1}} {{$paciente->pacienteApellido2}} {{$paciente->pacienteApellido3}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">N° de Expediente:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$paciente->pacienteCodigo}}  </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Edad: </label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$edad}} años </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Fecha de la consulta:</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static"> {{$fechaA}} </p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <!--/span-->
                                </div>
                                <!--/row-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>