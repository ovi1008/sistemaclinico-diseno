<div class="modal fade bs-example-modal-lg" aria-hidden="true" role="dialog" tabindex="-1" id="modal-create-consulta">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" 
					aria-label="Close">
	                     <span aria-hidden="true">×</span>
	                </button>
	                <h1 class="modal-title" style="color:darkorange">Advertencia</h1>
				</div>
				<div class="modal-header">
						<b>¿Seguro que desea finalizar la consulta, al guardarse no se podra realizar ningun alteracion?</b> 
				</div>
				<div class="modal-body">
					<h2 class="modal-title" align="center">Consulta:</h2>

					<div class="row">
							<div class="col-md-12">
								<div class="panel panel-info">
									<div class="panel-wrapper collapse in" aria-expanded="true">
										<div class="panel-body">
											<form class="form-horizontal" role="form">
												<div class="form-body">
													<h3 class="box-title">Informacion del paciente</h3>
													<hr class="m-t-0 m-b-40">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label col-md-3">Nombre:</label>
																<div class="col-md-9">
																	<p class="form-control-static">{{$paciente->pacienteNombre1}} {{$paciente->pacienteNombre2}} {{$paciente->pacienteNombre3}} </p>
																</div>
															</div>
														</div>
														<!--/span-->
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label col-md-3">Apellido</label>
																<div class="col-md-9">
																	<p class="form-control-static">{{$paciente->pacienteApellido1}} {{$paciente->pacienteApellido2}} {{$paciente->pacienteApellido3}}</p>
																</div>
															</div>
														</div>
														<!--/span-->
													</div>
													<!--/row-->
													<div class="row">
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label col-md-3">N° de Expediente:</label>
																<div class="col-md-9">
																	<p class="form-control-static"> {{$paciente->pacienteCodigo}}  </p>
																</div>
															</div>
														</div>
														<!--/span-->
														<div class="col-md-6">
															<div class="form-group">
																<label class="control-label col-md-3">Edad: </label>
																<div class="col-md-9">
																	<p class="form-control-static"> {{$edad}} años </p>
																</div>
															</div>
														</div>
														<!--/span-->
													</div>
													<!--/row-->
													<div class="row">
														<div class="col-md-8">
															<div class="form-group">
																<label class="control-label col-md-3">Fecha de la consulta:</label>
																<div class="col-md-9">
																	<p class="form-control-static"> {{$fechaA}} </p>
																</div>
															</div>
														</div>
														<!--/span-->
														<!--/span-->
													</div>
													<!--/row-->
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="row">
								<div class="col-md-12">
									<div class="panel panel-info">
										<div class="panel-wrapper collapse in" aria-expanded="true">
											<div class="panel-body">
												<form class="form-horizontal" role="form">
													<div class="form-body">
														<h3 class="box-title">Signos Vitales</h3>
														<hr class="m-t-0 m-b-40">
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label col-md-3">Pulso:</label>
																	<div class="col-md-9">
																		<p class="form-control-static"> {{$signo->preparacionPulso}} mmHg </p>
																	</div>
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label col-md-3">Temperatura</label>
																	<div class="col-md-9">
																		<p class="form-control-static"> {{$signo->preparacionTemperatura}} °C  </p>
																	</div>
																</div>
															</div>
															<!--/span-->
														</div>
														<!--/row-->
														<div class="row">
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label col-md-3">Peso</label>
																	<div class="col-md-9">
																		<p class="form-control-static"> {{$signo->preparacionPeso}} Lb </p>
																	</div>
																</div>
															</div>
															<!--/span-->
															<div class="col-md-6">
																<div class="form-group">
																	<label class="control-label col-md-3">Altura: </label>
																	<div class="col-md-9">
																		<p class="form-control-static"> {{$signo->preparacionAltura}} cm </p>
																	</div>
																</div>
															</div>
															<!--/span-->
														</div>
														<!--/row-->
														<!--/row-->
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>





					<h4 class="modal-title">Consulta Por:</h4>
					<b id="parte01C"></b>
					<h4 class="modal-title">Presente Enfermedad:</h4>
					<b id="parte02C"></b>
					<h4 class="modal-title">Antecedentes Personales:</h4>
					<b id="parte03C"></b>
					<h4 class="modal-title">Antecedentes Familiares:</h4>
					<b id="parte04C"></b>
					<h4 class="modal-title">Exploracion Clinica:</h4>
					<b id="parte05C"></b>
					<h4 class="modal-title">Diagnostico Principal:</h4>
					<b id="parte06C"></b>
					<h4 class="modal-title">Enfermedad Principal:</h4>
					<b id="parte06AC"></b>
					<h4 class="modal-title">Otros Diagnosticos:</h4>
					<b id="parte07C"></b>
					<h4 class="modal-title">Enfermedad Secundaria 1:</h4>
					<b id="parte07AC"></b>
					<h4 class="modal-title">Enfermedad Secundaria 2:</h4>
					<b id="parte07BC"></b>
					<h4 class="modal-title">Tratamiento:</h4>
					<b id="parte08C"></b>
					<h4 class="modal-title">Observaciones:</h4>
					<b id="parte09C"></b>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="btn btn-success">Confirmar</button>
				</div>
			</div>
		</div>
</div>