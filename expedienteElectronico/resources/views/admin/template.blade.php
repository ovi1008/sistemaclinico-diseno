<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('plugins/images/favicon.png')}}">
    <title>Expediente Electronico - @yield('tituloTab','Inicio')</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css')}}" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="{{asset('plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('plugins/bower_components/owl.carousel/owl.carousel.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('plugins/bower_components/owl.carousel/owl.theme.default.css')}}" rel="stylesheet" type="text/css" />

    

    <!-- Daterange picker plugins css -->
    <link href="{{asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('pluginsPlantilla/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bower_components/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css">
    <!-- Menu CSS -->
    <link href="{{asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{asset('pluginsPlantilla/css/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('pluginsPlantilla/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    
    <script src="{{asset('js/validaciones.js')}}"></script>
 


    <!-- color CSS -->
    <link href="{{asset('pluginsPlantilla/css/colors/megna-dark.css')}}" id="theme" rel="stylesheet">
    @yield('urlJavaCssPersonalizado')


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="{{ asset('js/efecto.js') }}" defer></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                    <a class="logo" href="{{URL::action('HomeController@index')}}">
                    <img src="{{asset('img/clinica/'.$clinicaHome->clinicaLogo)}}" align="left" width="220" height="64" alt="home"  />
                    <!-- Logo -->
                    
                        <!-- Logo icon image, you can use font-icon also --><b>
                        <!--This is dark logo icon-->
                     </b>
                        <!-- Logo text image you can use text also --><span class="hidden-xs">
                     </span> </a>

                <!-- /Logo -->
                <!-- Search input and Toggle icon -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="javascript:void(0)"> <img src="{{asset('img/users/'.Auth::user()->usuarioAvatar)}}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->usuarioNombre ." ". Auth::user()->usuarioApellido}}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">                                
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{asset('img/users/'.Auth::user()->usuarioAvatar)}}" alt="user" /></div>
                                    <div class="u-text">
                                        <h4>{{ Auth::user()->usuarioNombre." ". Auth::user()->usuarioApellido}}</h4>
                                        <p class="text-muted">{{ Auth::user()->email }}</p></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            @if((Auth::user()->usuarioTipo)=="0")
                            <li><a href="{{URL::action('ClinicaController@edit',1)}}"><i class="ti-settings"></i> Configurar Clinica</a></li>
                            @endif
                            <li><a href="javascript:void(0)" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                 <i class="fa fa-power-off"></i> Cerrar Sesion</a>
                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
                <ul class="nav" id="side-menu">
                    <li> <a href="{{URL::action('HomeController@index')}}" class="waves-effect"><i  class="mdi mdi-home fa-fw"></i> <span class="hide-menu">Inicio</span></a> </li>
                    <li class="devider"></li>
                    @if((Auth::user()->usuarioTipo)=="0")
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-table fa-fw"></i> <span class="hide-menu">Administracion<span class="fa arrow"></span><span class="label label-rouded label-danger pull-right">9</span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{URL::action('UserController@index')}}"><i class="ti-user fa-fw"></i><span class="hide-menu">Usuarios</span></a></li>
                            <li><a href="{{URL::action('Cie10Controller@index')}}"><i class="ti-world  fa-fw"></i><span class="hide-menu">CIE10</span></a></li>
                            <li><a href="{{URL::action('HorarioController@index')}}"><i class="ti-alarm-clock  fa-fw"></i><span class="hide-menu">Asignacion de Horarios</span></a></li>
                            <li><a href="{{URL::action('EspecialidadController@index')}}"><i class="fa fa-user-md fa-fw"></i><span class="hide-menu">Especialidades</span></a></li>
                            <li><a href="{{URL::action('AsuetoController@index')}}"><i class="mdi mdi-calendar-today fa-fw"></i><span class="hide-menu">Dias Asuetos</span></a></li>
                        </ul>
                    </li>
                    @endif
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-clipboard-text fa-fw"></i> <span class="hide-menu">Paciente<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{URL::action('PacienteController@index')}}"><i class="ti-layout-list-thumb fa-fw"></i><span class="hide-menu">Ficha Clinica</span></a></li>
                            @if ((Auth::user()->usuarioTipo==0)||(Auth::user()->usuarioTipo==2))
                            <li><a href="{{ route('consulta.Listar',['id' => Auth::user()->id])}}"><i class="mdi mdi-hospital fa-fw"></i><span class="hide-menu">Dar Consulta</span></a></li>
                            @else
                            @endif
                        </ul>
                    </li>
                    <li> <a href="{{URL::action('ClinicaController@index')}}" class="waves-effect"><i  class="mdi mdi-information fa-fw"></i> <span class="hide-menu">Clinica</span></a> </li>
                        
                    
        
                    
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="text-center">
                        <h1  class="page-title text-info"><strong>@yield('tituloP','Starter Page')</strong></h1> </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                                @include('sweetalert::alert')
                            @yield('contenido')
                            {{-- <h3 class="box-title">Blank Starter page</h3>--}}</div> 
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> 2019 &copy; Expediente Electronico por DiseñoTeam05 </footer>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->

    <script src="{{asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('pluginsPlantilla/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{asset('pluginsPlantilla/js/jquery.slimscroll.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{asset('pluginsPlantilla/js/waves.js')}}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{asset('pluginsPlantilla/js/custom.min.js')}}"></script>
    <!--Style Switcher -->
    <script src="{{asset('plugins/bower_components/styleswitcher/jQuery.style.switcher.js')}}"></script>

    <script src="{{asset('plugins/bower_components/datatables/jquery.dataTables.min.js')}}"></script>

    <script src="{{asset('plugins/bower_components/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js')}}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/moment/moment.js')}}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js')}}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js')}}"></script>
    <script src="{{asset('plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js')}}"></script>
    <script src="{{asset('plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js')}}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>

    <script src="{{asset('plugins/bower_components/owl.carousel/owl.carousel.min.js')}}"></script>
    <script src="{{asset('plugins/bower_components/owl.carousel/owl.custom.js')}}"></script>
    <script src="{{asset('pluginsPlantilla/js/mask.js')}}"></script>

 
    
    @yield('javaPersonalizado')
</body>

</html>