@extends('admin.template2')
@section('contenido')
<form class="form-horizontal form-material" method="POST" action="{{ route('password.update') }}">
  @csrf

  <input type="hidden" name="token" value="{{ $token }}">

  <div class="form-group ">
    <div class="col-xs-12">
            <a href="/" class="text-center db"><img src="{{asset('img/clinica/'.$clinicaHome->clinicaLogo)}}" width="399" height="109" alt="Home" /><br/></a>  
            <br><br>
      <h3>Restaurar Contraseña</h3>
    </div>
  </div>
  <div class="form-group ">
    <div class="col-xs-12">
      <input id="email" type="email" placeholder="correo@dominio.com" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
      @error('email')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
    </div>
  </div>

  <div class="form-group ">
    <div class="col-xs-12">
      <input id="password" type="password" placeholder="contraseña" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
      @error('password')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
    </div>
  </div>

  <div class="form-group ">
    <div class="col-xs-12">
      <input id="password-confirm" type="password"  placeholder="confirmar contraseña" class="form-control" name="password_confirmation" required autocomplete="new-password">
    </div>
  </div>

  <div class="form-group text-center m-t-20">
    <div class="col-xs-12">
      <button   class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Enviar</button>
    </div>
  </div>

</form>
@endsection
