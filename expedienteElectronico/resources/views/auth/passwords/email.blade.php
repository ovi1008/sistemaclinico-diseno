@extends('admin.template2')
@section('contenido')
@if (session('status'))
                        <div class="alert alert-success" role="alert">
                          {{ session('status') }}
                        </div>
                    @endif
<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-group ">
          <div class="col-xs-12">
                <a href="/" class="text-center db"><img src="{{asset('img/clinica/'.$clinicaHome->clinicaLogo)}}" width="399" height="109" alt="Home" /><br/></a>  
                <br><br>
            <h3>Recuperar Contraseña</h3>
            <p class="text-muted">Introduzca su correo y se le enviaran las indicaciones</p>
          </div>
        </div>
        
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control @error('email') is-invalid @enderror" type="email" name="email" placeholder="correo@dominio.com" value="{{ old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
            <strong class="text-danger">{{ $message }}</strong>
            </span>
            @enderror
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button id="email"  class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Resetear</button>
          </div>
        </div>
      </form>
@endsection


