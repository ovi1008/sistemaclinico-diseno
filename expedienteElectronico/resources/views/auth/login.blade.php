@extends('admin.template2')

@section('contenido')
<form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
        @csrf
        <a href="/" class="text-center db"><img src="{{asset('img/clinica/'.$clinicaHome->clinicaLogo)}}" width="399" height="109" alt="Home" /><br/></a>  
        <div class="form-group m-t-40">
            <div class="col-xs-12">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"  required="" placeholder="Correo" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                <strong class="text-danger">{{ $message }}</strong>
                </span>
                @enderror
            </div>
      </div>
      <div class="form-group">
        <div class="col-xs-12">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="contraseña" name="password" required autocomplete="current-password">
          @error('password')
          <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12">
          <div class="checkbox checkbox-primary pull-left p-t-0">
          <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          <label for="checkbox-signup"> Recuerdame </label>
          </div>
          <a href="{{ route('password.request') }}" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>¿Olvido la contraseña?</a> </div>
      </div>
      <div class="form-group text-center m-t-20">
        <div class="col-xs-12">
          <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"
          {{ __('Login') }}>Iniciar Sesion</button>
        </div>
      </div>
    
@endsection


