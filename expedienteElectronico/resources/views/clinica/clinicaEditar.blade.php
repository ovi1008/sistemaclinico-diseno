@extends('admin.template')

@section('tituloTab')
Clinica 
@endsection

@section('tituloP')
Actualizar Datos de Clinica
@endsection

@section('contenido')
<p class="text-muted m-b-30 font-13"><code>Puede modificar solamente el campo que ustede desea y dejar los demas sin tocar</code></p>

{!!Form::model($clinica,['method'=>'PATCH','route'=>['clinica.update',$clinica->id],'files'=>'true'])!!}
{!! Form::hidden('id', $clinica->id) !!}
{{Form::token()}}


            <div class="form-group{{ $errors->has('clinicaNombre') ? ' has-error' : '' }}">
                    <label for="inputEmail" class="control-label">Nombre</label>
                    <input id="clinicaNombre" type="text" class="form-control @error('clinicaNombre') is-invalid @enderror" name="clinicaNombre" value="{{ old('clinicaNombre',$clinica->clinicaNombre) }}" required autocomplete="clinicaNombre">       
                    <div class="help-block with-errors">
                        @error('clinicaNombre')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                    </div>
                </div>

                <div class="form-group{{ $errors->has('clinicaDireccion') ? ' has-error' : '' }}">
                        <label for="inputEmail" class="control-label">Direccion</label>
                        <input id="clinicaDireccion" type="text" class="form-control @error('clinicaDireccion') is-invalid @enderror" name="clinicaDireccion" value="{{ old('clinicaDireccion',$clinica->clinicaDireccion) }}" required autocomplete="clinicaDireccion">        
                        <div class="help-block with-errors">
                            @error('clinicaDireccion')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                        </div>
                    </div>
    



        <div class="form-group{{ $errors->has('clinicaTelefono') ? ' has-error' : '' }}">
                <label for="inputEmail" class="control-label">Telefono</label>
                <input id="clinicaTelefono" type="text" class="form-control @error('clinicaTelefono') is-invalid @enderror" name="clinicaTelefono" value="{{ old('clinicaTelefono',$clinica->clinicaTelefono) }}" required autocomplete="clinicaTelefono" onkeypress="return duiFormat(event)" onpaste="return false" maxlength="9" pattern="([0-9]{4}([\-][0-9]{4})?)" title="2222-2222">                      
                <div class="help-block with-errors">
                    @error('clinicaTelefono')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                </div>
            </div>
            <div class="form-group{{ $errors->has('clinicaDescripcion') ? ' has-error' : '' }}">
                <label for="inputEmail" class="control-label">Descripcion</label>
                <input id="clinicaDescripcion" type="text" class="form-control @error('clinicaDescripcion') is-invalid @enderror" name="clinicaDescripcion" value="{{ old('clinicaDescripcion',$clinica->clinicaDescripcion) }}" required autocomplete="clinicaDescripcion">
                <div class="help-block with-errors">
                    @error('clinicaDescripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                </div>
            </div>

            <div class="form-group{{ $errors->has('clinicaImagen') ? ' has-error' : '' }}">
                <label for="input_imagen">Imagen Clinica (png, jpg, jpeg)</label>
                <input id="clinicaImagen" type="file" class="form-control @error('clinicaImagen') is-invalid @enderror" name="clinicaImagen" value="{{ old('clinicaImagen',$clinica->clinicaImagen) }}"  autocomplete="clinicaImagen">
                @if(($clinica->clinicaImagen)!=" ")
                      <img class="img-thumbnail" src="{{asset('img/clinica/'.$clinica->clinicaImagen)}}" alt="{{$clinica->clinicaNombre}}" height="110px" width="110px">
                   @endif
                <div class="help-block with-errors">
                    @error('clinicaImagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                </div>
             </div>
             <div class="form-group{{ $errors->has('clinicaLogo') ? ' has-error' : '' }}">
                    <label for="input_imagen">Logo Clinica (png, jpg, jpeg)</label>
                    <input id="clinicaLogo" type="file" class="form-control @error('clinicaLogo') is-invalid @enderror" name="clinicaLogo" value="{{ old('clinicaLogo',$clinica->clinicaLogo) }}"  autocomplete="clinicaLogo">
                    @if(($clinica->clinicaImagen)!=" ")
                          <img class="img-thumbnail" src="{{asset('img/clinica/'.$clinica->clinicaLogo)}}" alt="{{$clinica->clinicaNombre}}" height="110px" width="110px">
                       @endif
                    <div class="help-block with-errors">
                        @error('clinicaLogo')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                    </div>
                 </div>
    <div class="form-group">
        <button type="submit" name="updateClinica"class="btn btn-block btn-primary">Actualizar</button>
    </div>
{!!Form::close()!!}	


@endsection
