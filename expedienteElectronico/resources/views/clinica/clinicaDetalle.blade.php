@extends('admin.template')

@section('tituloTab')
Clinica  
@endsection

@section('tituloP')
Información de la clinica
@endsection

@section('contenido')

<div class="">
<h1 class="m-b-0 m-t-0">{{$clinica->clinicaNombre}}</h1>
    <hr>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="white-box text-center"> <img src="{{asset('img/clinica/'.$clinica->clinicaImagen)}}" class="img-responsive" /> </div>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-6">
            <h4 class="box-title m-t-40">Descripcion de la clinica</h4>
        <p>{{$clinica->clinicaDescripcion}}</p>
            <h3 class="box-title m-t-40">Detalles</h3>
            <ul class="list-icons">
                <li><i class="fa fa-check text-success"></i>Direccion: &nbsp; {{$clinica->clinicaDireccion}}</li>
            <li><i class="fa fa-check text-success"></i>Telefono: &nbsp; {{$clinica->clinicaTelefono}}</li>
            </ul>
        </div>
    </div>
</div>


@endsection
