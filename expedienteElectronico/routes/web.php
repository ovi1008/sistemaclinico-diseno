<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Rutas de login
Auth::routes();

//Rutas de home
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


//Rutas de crud Uusario
Route::resource('/user','UserController');
#Mostrar detalle usuario
Route::get('/user/{user}/detalle', 'UserController@detalle')->name('user.detalle');
#Listados de Usuario no activos
Route::get('/usuario2', 'UserController@show')->name('user2');
#Cambiar Estado de usuario
Route::post('/usuario/estado/{user}','UserController@estadoC');
Route::post('/usuario/estado2/{user}','UserController@estadoC2');

//Rutas de crud clinica
Route::resource('/clinica','ClinicaController');

//Rutas de crud paciente
Route::resource('/paciente','PacienteController');
//LLena los municipios
Route::get('paciente/paciente/departamento/{id}', 'PacienteController@obtenerMunicipios');
Route::get('paciente/{id1}/paciente/departamento/{id}', 'PacienteController@obtenerMunicipios2');

//Rutas de crud paciente
Route::resource('/preparacion','PreparacionController');
Route::get('/preparacion/crear/{id}','PreparacionController@crearSig')->name('preparacion.Crear');
Route::get('/preparacion/editar/{id}','PreparacionController@editSig')->name('preparacion.Editar');
Route::get('/preparacion/detalle/{id}','PreparacionController@detaSig')->name('preparacion.Detalle');
Route::get('/preparacion/listado/{id}','PreparacionController@listarSignos')->name('preparacion.Listar');

//Rutas de crud consulta
Route::resource('/consulta','ConsultaController');
Route::get('/consulta/crear/{id}','ConsultaController@crearC')->name('consulta.Crear');
Route::get('/consulta/listado/{id}','ConsultaController@listarC')->name('consulta.Listar');
Route::get('consulta/crear/consulta/enfermedadP/{nombre}', 'ConsultaController@obtenerEnfermedades');


//Rutas de crud cie10
Route::resource('/cie10','Cie10Controller');
Route::get('/cie10/editar/{id}','Cie10Controller@edith')->name('cie10.Editar');

//Rutas de crud horario
Route::resource('/horarios','HorarioController');



//Rutas de crud asueto
Route::resource('/asueto','AsuetoController');
//Route::get('/cie10/editar/{id}','Cie10Controller@edith')->name('cie10.Editar');

//Rutas de crud especialidad
Route::resource('/especialidad','EspecialidadController');
//Route::get('/cie10/editar/{id}','Cie10Controller@edith')->name('cie10.Editar');

//Rutas de crud receta
Route::resource('/receta','RecetaController');
//Route::get('/receta/editar/{id}','recetaController@edith')->name('receta.Editar');

//Rutas de crud referencia
Route::resource('/referencia','ReferenciaController');

//Rutas de crud boleta de examen
Route::resource('/boletaexam','boletaexamController');

//Rutas de crud citas
Route::resource('/citas','CitasController');
