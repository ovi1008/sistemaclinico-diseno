<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Pacientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pacienteNombre1');
            $table->string('pacienteCodigo')->unique();
            $table->string('pacienteNombre2')->nullable();
            $table->string('pacienteNombre3')->nullable();
            $table->string('pacienteApellido1');
            $table->string('pacienteApellido2')->nullable();
            $table->string('pacienteApellido3')->nullable();
            $table->enum('pacienteSexo',['0','1']);
            $table->string('pacienteReligion')->nullable();;
            $table->string('pacienteFechaNacimiento');
            $table->string('pacienteTelefono',9);
            $table->string('pacienteDireccion');
            $table->string('pacienteProfesion')->nullable();
            $table->string('pacienteDui',10)->unique()->nullable();
            $table->string('pacienteNit',17)->unique()->nullable();
            $table->string('pacienteDepartamento');
            $table->string('pacienteMunicipio');
            $table->enum('pacienteEstadoCivil',['0','1','2','3']);;
            $table->string('pacientePersonaEmergencia')->nullable();
            $table->string('pacientePersonaEmergenciaTelefono',9)->nullable();
            $table->string('pacienteTipoDeSangre',15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
