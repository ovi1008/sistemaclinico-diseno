<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCie10Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cie10', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cie10Codigo')->unique();
            $table->string('cie10Descripcion');
            $table->enum('cie10Sexo',['0','1','2']);
            $table->string('cie10LimiteInferior')->nullable();
            $table->string('cie10LimiteSuperior')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cie10');
    }
}
