<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Preparaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preparaciones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idPaciente');
            $table->integer('preparacionPulso')->nullable();
            $table->decimal('preparacionTemperatura', 8, 1)->nullable();
            $table->decimal('preparacionPeso', 8, 1)->nullable();
            $table->decimal('preparacionAltura', 8, 1)->nullable();
            $table->string('preparacionAsignacion');
            $table->timestamps();
            $table->string("preparacionEstado",1)->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preparaciones');
    }
}
