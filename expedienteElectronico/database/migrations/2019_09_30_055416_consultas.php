<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Consultas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idPacientePreparacion');
            $table->string('consultaConsultaPor');
            $table->string('consultaPresentaEnfermedad')->nullable();
            $table->string('consultaAntecedentesPersonales')->nullable();
            $table->string('consultaAntecedentesFamiliares')->nullable();
            $table->string('consultaExploracionClinica');
            $table->string('consultaDiagnosticoPrincipal')->nullable();
            $table->string('consultaOtrosDiagnosticos')->nullable();
            $table->string('consultaTratamiento')->nullable();
            $table->string('consultaObservaciones')->nullable();
            $table->string('consultaEnfermedadP');
            $table->string('consultaEnfermedadS1')->nullable();
            $table->string('consultaEnfermedadS2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultas');
    }
}
