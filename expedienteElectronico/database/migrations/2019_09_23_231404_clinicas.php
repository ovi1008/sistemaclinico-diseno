<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Clinicas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('clinicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clinicaNombre');
            $table->string('clinicaDescripcion');
            $table->string('clinicaDireccion');
            $table->string('clinicaTelefono');
            $table->string('clinicaImagen')->default('clinica_default01.jpg');
            $table->string('clinicaLogo')->default('logoClinica.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicas');
    }
}
