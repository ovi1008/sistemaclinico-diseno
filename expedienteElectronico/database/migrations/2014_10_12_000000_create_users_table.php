<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('usuarioAvatar')->nullable()->default('usuario_default.png');
            $table->string('usuarioNombre');
            $table->string('usuarioNombre2')->nullable();
            $table->string('usuarioNombre3')->nullable();
            $table->string('usuarioApellido');
            $table->string('usuarioApellido2')->nullable();
            $table->string('usuarioApellido3')->nullable();
            $table->string('usuarioDui',10)->unique();
            $table->string('usuarioNit',17)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->enum('usuarioTipo',['0','1','2','3']);
            $table->string('usuarioJrv')->nullable();
            $table->string('idEspecialidad')->nullable();;
            $table->string('usuarioEstado',1)->default('1');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
