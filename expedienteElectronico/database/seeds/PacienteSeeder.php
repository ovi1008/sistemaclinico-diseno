<?php

use Illuminate\Database\Seeder;
use App\Paciente;
use Carbon\Carbon;

class PacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idA=0;
        $i2=0;
        
        for($i = 0; $i<=9;$i++){
            $fecha = Carbon::now();
            $codFecha=strtotime($fecha);
            $codStr=(string)$codFecha;

            $paciente1=Paciente::create([
                'pacienteNombre1'  => Str::random(6),
                'pacienteNombre2' => Str::random(6),
                'pacienteCodigo'=>$codStr.$idA,
                'pacienteNombre3' => '',
                'pacienteApellido1' => Str::random(6),
                'pacienteApellido2' => '',
                'pacienteApellido3' => '',
                'pacienteSexo' => '0',
                'pacienteReligion' => 'catolico', 
                'pacienteFechaNacimiento' => '9/09/2019',
                'pacienteTelefono' => '1111-1111',
                'pacienteDireccion'  => 'Santa Ana',
                'pacienteProfesion' => 'Ingeniero',
                'pacienteDui' => '22222222-'.$i, 
                'pacienteNit' =>'2222-222222-222-'.$i,
                'pacientePersonaEmergencia'=>'pruebaEmergencia',
                'pacientePersonaEmergenciaTelefono' =>'1111-1111',
                'pacienteTipoDeSangre' =>'O+',
                'pacienteDepartamento' =>'4',
                'pacienteMunicipio' =>'138',
                'pacienteEstadoCivil'=>(string)$i2,
                ]);
                $idA=$paciente1->id;
                $i2=$i2+1;
                if($i2==4){
                $i2=0;
            }
        }
        $i2=0;

        for($i = 0; $i<=9;$i++){
            $fecha = Carbon::now();
            $codFecha=strtotime($fecha);
            $codStr=(string)$codFecha;
            $paciente2=Paciente::create([
                'pacienteNombre1'  => Str::random(6),
                'pacienteNombre2' => Str::random(6),
                'pacienteCodigo'=>$codStr.$idA,
                'pacienteNombre3' => '',
                'pacienteApellido1' => Str::random(6),
                'pacienteApellido2' => '',
                'pacienteApellido3' => '',
                'pacienteSexo' => '1',
                'pacienteReligion' => 'catolico', 
                'pacienteFechaNacimiento' => '9/09/2019',
                'pacienteTelefono' => '1111-1111',
                'pacienteDireccion'  => 'Santa Ana',
                'pacienteProfesion' => 'Ingeniero',
                'pacienteDui' => '22222223-'.$i, 
                'pacienteNit' =>'2222-222222-223-'.$i,
                'pacientePersonaEmergencia'=>'pruebaEmergencia',
                'pacientePersonaEmergenciaTelefono' =>'1111-1111',
                'pacienteTipoDeSangre' =>'A+',
                'pacienteDepartamento' =>'4',
                'pacienteMunicipio' =>'138',
                'pacienteEstadoCivil'=>(string)$i2,
                ]);
                $idA=$paciente2->id;
                $i2=$i2+1;
                if($i2==4){
                $i2=0;
        }
    }
    }
}
