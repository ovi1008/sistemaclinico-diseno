<?php

use Illuminate\Database\Seeder;
use App\Preparacion;

class PreparacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for($i = 1; $i<=8;$i++){
            Preparacion::create([
                'idPaciente'  => 1,
                'preparacionPulso' => 30,
                'preparacionTemperatura' => 37.5,
                'preparacionPeso' => 180,
                'preparacionAltura' => 180 + $i,
                'preparacionAsignacion' =>'1',
                'preparacionEstado' => '0'
                ]);


        }
        for($i = 1; $i<=8;$i++){
            Preparacion::create([
                'idPaciente'  => 2,
                'preparacionPulso' => 30,
                'preparacionTemperatura' => 37.5,
                'preparacionPeso' => 180,
                'preparacionAltura' => 180 + $i,
                'preparacionAsignacion' =>'2',
                'preparacionEstado' => '1'
                ]);
            
        }
    }
}
