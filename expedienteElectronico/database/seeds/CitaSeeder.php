<?php

use Illuminate\Database\Seeder;
use App\Cita;

class CitaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   //$idDoctor = 1;
        $idPaciente = 1;

        for($i = 1; $i<=9; $i++){
            if($i % 2 ==0){
                $idDoctor ="1";
            }
            else{
                $idDoctor ="2";
            }
            Cita::create([
                'idDoctor' => $idDoctor,
                'idPaciente' => $i,
                'fechaCita' => (string)$i.'/29/2019',
                'horaCita' => '1'.(string)$i.':00'
                ]);
        }
    }
}

