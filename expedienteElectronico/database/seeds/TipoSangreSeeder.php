<?php

use App\TipoSangre;
use Illuminate\Database\Seeder;

class TipoSangreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        TipoSangre::create([
            'nombreSangre'  => 'O-',
            ]);

        TipoSangre::create([
            'nombreSangre'  => 'O+',
            ]);

        TipoSangre::create([
            'nombreSangre'  => 'A-',
            ]);
        TipoSangre::create([
            'nombreSangre'  => 'A+',
            ]);
        TipoSangre::create([
            'nombreSangre'  => 'B-',
            ]);
        TipoSangre::create([
            'nombreSangre'  => 'B+',
            ]);
        TipoSangre::create([
            'nombreSangre'  => 'AB-',
            ]);
        TipoSangre::create([
            'nombreSangre'  => 'AB+',
            ]);
    }
}
