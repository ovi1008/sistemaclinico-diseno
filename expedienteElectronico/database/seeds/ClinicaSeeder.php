<?php

use Illuminate\Database\Seeder;
use App\Clinica;

class ClinicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Clinica::create([
            'clinicaNombre'  => 'clinicaPrueba',
            'clinicaDescripcion' => 'Descripcion de clinica de prueba',
            'clinicaDireccion' => 'Santa Tecla de prueba',
            'clinicaTelefono' => '1111-1111'
            ]);
    }
}
