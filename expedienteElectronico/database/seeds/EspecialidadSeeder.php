<?php

use Illuminate\Database\Seeder;
use App\Especialidad;

class EspecialidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Especialidad::create([
            'especialidadNombre'  => 'Pediatria',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Cardiologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'General',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Neurologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Nutriologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Psiquiatria',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Toxicologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Oncologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Reumatologia',
        ]);
        Especialidad::create([
            'especialidadNombre'  => 'Geriatria',
        ]);
    }
}
