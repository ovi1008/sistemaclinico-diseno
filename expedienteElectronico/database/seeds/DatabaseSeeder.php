<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EspecialidadSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ClinicaSeeder::class);
        $this->call(PacienteSeeder::class);
        $this->call(PreparacionSeeder::class);
        $this->call(ConsultaSeeder::class);
        $this->call(HorarioSeeder::class);
        $this->call(CitaSeeder::class);
        $this->call(TipoSangreSeeder::class);
    }
}
