<?php

use Illuminate\Database\Seeder;
use App\Consulta;
class ConsultaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i<=8;$i++){
            Consulta::create([
                'idPacientePreparacion'  => $i,
                'consultaConsultaPor' => Str::random(50),
                'consultaPresentaEnfermedad' => Str::random(50),
                'consultaAntecedentesPersonales' => Str::random(50),
                'consultaAntecedentesFamiliares' => Str::random(50),
                'consultaExploracionClinica' => Str::random(50),
                'consultaDiagnosticoPrincipal' => Str::random(50),
                'consultaOtrosDiagnosticos' =>Str::random(50),
                'consultaTratamiento' => Str::random(50),
                'consultaObservaciones' => Str::random(50),
                'consultaEnfermedadP' => Str::random(50)
                ]);

        }

    }


}

