<?php

use Illuminate\Database\Seeder;
use App\Asueto;

class AsuetoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Asueto::create([
            'asuetoNombreDia'  => 'primero de enero',
            'asuetoFecha' => '01/01',
            ]);

        Asueto::create([
            'asuetoNombreDia'  => 'dia del Trabajo',
            'asuetoFecha' => '01/05',
            ]);
        Asueto::create([
            'asuetoNombreDia'  => 'Dia de la madre',
            'asuetoFecha' => '10/05',
            ]);

        Asueto::create([
            'asuetoNombreDia'  => 'Dia del padre',
            'asuetoFecha' => '17/06',
            ]);
        Asueto::create([
            'asuetoNombreDia'  => 'Fiesta del Divino Salvador del Mundo',
            'asuetoFecha' => '06/07',
            ]);
        Asueto::create([
            'asuetoNombreDia'  => 'Dia de la independencia',
            'asuetoFecha' => '15/09',
            ]);
        Asueto::create([
            'asuetoNombreDia'  => 'Dia de Difuntos',
            'asuetoFecha' => '02/11',
            ]);
        Asueto::create([
            'asuetoNombreDia'  => 'Navidad',
            'asuetoFecha' => '25/12',
            ]);
    }
}
