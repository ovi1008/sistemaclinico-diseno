<?php

use Illuminate\Database\Seeder;
use App\Horario;
class HorarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   // 'horarioDoctorId', 'horarioHora', 'horarioPaciente',
        Horario::create([
            'horarioDoctorId' => 1,
            'horarioHoraI' => "08:00",
            'horarioHoraF' => "16:00",
            'horarioPaciente' => "15" 
        ]);


        Horario::create([
            'horarioDoctorId' => 2,
            'horarioHoraI' => "7:00",
            'horarioHoraF' => "15:00",
            'horarioPaciente' => "25" 
        ]);
    }
}
