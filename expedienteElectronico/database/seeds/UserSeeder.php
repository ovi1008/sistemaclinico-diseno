<?php

use Illuminate\Database\Seeder;
use App\User;
 

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       User::create([
        'usuarioNombre'  => 'prueba',
        'usuarioNombre2' => 'prueba',
        'usuarioNombre3' => '',
        'usuarioApellido' => 'prueba',
        'usuarioApellido2' => '',
        'usuarioApellido3' => '',
        'usuarioDui' => '11111111-1',
        'usuarioNit' => '1111-111111-111-1',
        'email'     => 'prueba@gmail.com',
        'usuarioTipo' => '0',
        'usuarioJrv'  => '111',
        'idEspecialidad'  => '3',
        'usuarioEstado' => '1',
        'password' => bcrypt('admin1234')
        ]);

        User::create([
            'usuarioNombre'  => 'prueba222',
            'usuarioNombre2' => 'prueb22222',
            'usuarioNombre3' => '',
            'usuarioApellido' => 'prueba444',
            'usuarioApellido2' => '',
            'usuarioApellido3' => '',
            'usuarioDui' => '11116511-1',
            'usuarioNit' => '1111-111561-111-1',
            'email'     => 'prueba345@gmail.com',
            'usuarioTipo' => '2',
            'usuarioJrv'  => '111',
            'idEspecialidad'  => '7',
            'usuarioEstado' => '1',
            'password' => bcrypt('admin1234')
            ]);

        $i2=1;
        for($i = 0; $i<=9;$i++){
            
            User::create([
                'usuarioNombre'  => Str::random(6),
                'usuarioNombre2' => Str::random(6),
                'usuarioNombre3' => '',
                'usuarioApellido' => Str::random(6),
                'usuarioApellido2' => '',
                'usuarioApellido3' => '',
                'usuarioDui' => '11111112-'.$i,
                'usuarioNit' => '1111-111111-112-'.$i, 
                'email'     => Str::random(6).$i.'@gmail.com',
                'usuarioTipo' => (string)$i2,
                'usuarioJrv'  => '11'.$i,
                'idEspecialidad'  => ''.$i+1,
                'usuarioEstado' => '1',
                'password' => bcrypt('admin1234'),
                ]);
            $i2=$i2+1;
            if($i2==4){
                $i2=1;
            }
        }
        
        $i2=1;        
        for($i = 0; $i<=9;$i++){
            
            User::create([
                'usuarioNombre'  => Str::random(6),
                'usuarioNombre2' => Str::random(6),
                'usuarioNombre3' => '',
                'usuarioApellido' => Str::random(6),
                'usuarioApellido2' => '',
                'usuarioApellido3' => '',
                'usuarioDui' => '11111113-'.$i,
                'usuarioNit' => '1111-111111-113-'.$i, 
                'email'     => Str::random(6).$i.'@gmail.com',
                'usuarioTipo' => (string)$i2,
                'usuarioJrv'  => '12'.$i,
                'idEspecialidad'  => ''.$i+1,
                'usuarioEstado' => '0',
                'password' => bcrypt('admin1234'),
                ]);
            $i2=$i2+1;
            if($i2==4){
                $i2=1;
            }
        } 
    }
    
}
